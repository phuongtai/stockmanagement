﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using test1.Services;
using test1.ViewModels;
using AutoMapper;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors;

namespace test1
{
  public partial class ucOrderHistory : UserControl, INotifyPropertyChanged
  {
    private int orderId;
    public event PropertyChangedEventHandler PropertyChanged;

    public int OrderId
    {
      get { return orderId; }
      set
      {
        orderId = value;
        InvokePropertyChanged(new PropertyChangedEventArgs("OrderId"));
      }
    }

    public void InvokePropertyChanged(PropertyChangedEventArgs e)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      if (handler != null) handler(this, e);
    }

    public ucOrderHistory()
    {
      InitializeComponent();
    }


    //public ucOrderHistory(OrderService orderService, IMapper mapper)
    //{
    //  InitializeComponent();
    //  this.orderService = orderService;
    //  this.mapper = mapper;
    //}

    public void LoadData(OrderService orderService)
    {
      var data = orderService.GetOderHistory();
      mainBindingSource.DataSource = data;
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      if (gvProduct.GetSelectedRows().Count() > 0)
      {
        var data = (OrderHistoryViewModel)gvProduct.GetRow(gvProduct.GetSelectedRows()[0]);
        this.OrderId = data.OrderId;
      }
      else
        MessageBox.Show("Hãy chọn 1 dòng để sữa đổi !", "Thông Báo");

    }

    private void GrdOrderHistory_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      if (gvProduct.GetSelectedRows().Count() > 0)
      {
        var data = (OrderHistoryViewModel)gvProduct.GetRow(gvProduct.GetSelectedRows()[0]);
        this.OrderId = data.OrderId;
      }
    }
  }
}
