﻿namespace test1
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }


    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
      DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions9 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions10 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions11 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions12 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions13 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions14 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions15 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions16 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
      this.imgAdd = new DevExpress.Utils.ImageCollection(this.components);
      this.imgSave = new DevExpress.Utils.ImageCollection(this.components);
      this.imgDelete = new DevExpress.Utils.ImageCollection(this.components);
      this.NavMain = new DevExpress.XtraBars.Navigation.NavigationFrame();
      this.customerPage = new DevExpress.XtraBars.Navigation.NavigationPage();
      this.txtEmail = new DevExpress.XtraEditors.TextEdit();
      this.txtAddress = new DevExpress.XtraEditors.TextEdit();
      this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
      this.txtPhone = new DevExpress.XtraEditors.TextEdit();
      this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
      this.txtCustomer = new DevExpress.XtraEditors.TextEdit();
      this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
      this.lbl = new DevExpress.XtraEditors.LabelControl();
      this.productPage = new DevExpress.XtraBars.Navigation.NavigationPage();
      this.cboUnit = new DevExpress.XtraEditors.ImageComboBoxEdit();
      this.imgUnit = new DevExpress.Utils.ImageCollection(this.components);
      this.txtPrice = new DevExpress.XtraEditors.TextEdit();
      this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
      this.txtPaintCode = new DevExpress.XtraEditors.TextEdit();
      this.txtPaintName = new DevExpress.XtraEditors.TextEdit();
      this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      this.orderPage = new DevExpress.XtraBars.Navigation.NavigationPage();
      this.tabOrder = new DevExpress.XtraBars.Navigation.TabPane();
      this.tbOrderPage = new DevExpress.XtraBars.Navigation.TabNavigationPage();
      this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
      this.gridProduct = new DevExpress.XtraGrid.GridControl();
      this.gvProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
      this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.gc15 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.gc17 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
      this.pnDocumentManager = new DevExpress.XtraEditors.PanelControl();
      this.DateOrder = new DevExpress.XtraEditors.DateEdit();
      this.cboCustomer = new DevExpress.XtraEditors.SearchLookUpEdit();
      this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
      this.colCustomerName = new DevExpress.XtraGrid.Columns.GridColumn();
      this.colCustomerPhone = new DevExpress.XtraGrid.Columns.GridColumn();
      this.colCustomerAddress = new DevExpress.XtraGrid.Columns.GridColumn();
      this.btnReport = new DevExpress.XtraEditors.SimpleButton();
      this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
      this.txtNote = new System.Windows.Forms.TextBox();
      this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
      this.tbOrderHistory = new DevExpress.XtraBars.Navigation.TabNavigationPage();
      this.ucOrderHistory1 = new test1.ucOrderHistory();
      this.stockPage = new DevExpress.XtraBars.Navigation.NavigationPage();
      this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
      this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
      this.dtImport = new DevExpress.XtraEditors.DateEdit();
      this.txtStockCode = new DevExpress.XtraEditors.TextEdit();
      this.txtStockNote = new DevExpress.XtraEditors.TextEdit();
      this.txtPriceImport = new DevExpress.XtraEditors.TextEdit();
      this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
      this.txtQuantity = new DevExpress.XtraEditors.TextEdit();
      this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
      this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
      this.cboProduct = new DevExpress.XtraEditors.SearchLookUpEdit();
      this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
      this.reportPage = new DevExpress.XtraBars.Navigation.NavigationPage();
      this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
      this.grStock = new DevExpress.XtraGrid.GridControl();
      this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
      this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
      this.button1 = new System.Windows.Forms.Button();
      this.txtSearchBaoCao = new DevExpress.XtraEditors.TextEdit();
      this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
      this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
      this.hyperlinkLabelControl1 = new DevExpress.XtraEditors.HyperlinkLabelControl();
      this.btnpnNavigation = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
      this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
      this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
      this.gbInfo = new System.Windows.Forms.GroupBox();
      this.MainGrid = new DevExpress.XtraGrid.GridControl();
      this.mainBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
      this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
      this.txtSearch = new DevExpress.XtraEditors.TextEdit();
      this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
      this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
      this.btnPnAction = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
      this.alertControl1 = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
      this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
      this.styleController1 = new DevExpress.XtraEditors.StyleController(this.components);
      this.imgGallery = new DevExpress.Utils.ImageCollection(this.components);
      gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
      gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
      ((System.ComponentModel.ISupportInitialize)(this.imgAdd)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgSave)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgDelete)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.NavMain)).BeginInit();
      this.NavMain.SuspendLayout();
      this.customerPage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtCustomer.Properties)).BeginInit();
      this.productPage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.cboUnit.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgUnit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPrice.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPaintCode.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPaintName.Properties)).BeginInit();
      this.orderPage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.tabOrder)).BeginInit();
      this.tabOrder.SuspendLayout();
      this.tbOrderPage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
      this.groupControl2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gridProduct)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvProduct)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
      this.groupControl1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pnDocumentManager)).BeginInit();
      this.pnDocumentManager.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.DateOrder.Properties.CalendarTimeProperties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.DateOrder.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cboCustomer.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
      this.tbOrderHistory.SuspendLayout();
      this.stockPage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
      this.groupControl4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dtImport.Properties.CalendarTimeProperties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dtImport.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStockCode.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStockNote.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPriceImport.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtQuantity.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.cboProduct.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
      this.reportPage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
      this.groupControl5.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grStock)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
      this.groupControl6.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtSearchBaoCao.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
      this.panelControl1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
      this.panelControl2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
      this.splitContainerControl1.SuspendLayout();
      this.gbInfo.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.MainGrid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.mainBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
      this.groupControl3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
      this.panelControl4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.styleController1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgGallery)).BeginInit();
      this.SuspendLayout();
      // 
      // gridColumn5
      // 
      gridColumn5.Caption = "ID";
      gridColumn5.FieldName = "ID";
      gridColumn5.MinWidth = 23;
      gridColumn5.Name = "gridColumn5";
      gridColumn5.Width = 87;
      // 
      // gridColumn6
      // 
      gridColumn6.AppearanceCell.Options.UseTextOptions = true;
      gridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      gridColumn6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
      gridColumn6.AppearanceHeader.Options.UseTextOptions = true;
      gridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      gridColumn6.Caption = "Mã Sản Phẩm";
      gridColumn6.FieldName = "Code";
      gridColumn6.MinWidth = 24;
      gridColumn6.Name = "gridColumn6";
      gridColumn6.OptionsColumn.AllowEdit = false;
      gridColumn6.OptionsColumn.ReadOnly = true;
      gridColumn6.Visible = true;
      gridColumn6.VisibleIndex = 1;
      gridColumn6.Width = 100;
      // 
      // imgAdd
      // 
      this.imgAdd.ImageSize = new System.Drawing.Size(64, 64);
      this.imgAdd.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgAdd.ImageStream")));
      this.imgAdd.Images.SetKeyName(0, "add1.png");
      this.imgAdd.Images.SetKeyName(1, "add.png");
      // 
      // imgSave
      // 
      this.imgSave.ImageSize = new System.Drawing.Size(64, 64);
      this.imgSave.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgSave.ImageStream")));
      this.imgSave.Images.SetKeyName(0, "save.png");
      this.imgSave.Images.SetKeyName(1, "save_color.png");
      // 
      // imgDelete
      // 
      this.imgDelete.ImageSize = new System.Drawing.Size(64, 64);
      this.imgDelete.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgDelete.ImageStream")));
      this.imgDelete.Images.SetKeyName(0, "delete.png");
      this.imgDelete.Images.SetKeyName(1, "Delete_color.png");
      // 
      // NavMain
      // 
      this.NavMain.Controls.Add(this.customerPage);
      this.NavMain.Controls.Add(this.productPage);
      this.NavMain.Controls.Add(this.orderPage);
      this.NavMain.Controls.Add(this.stockPage);
      this.NavMain.Controls.Add(this.reportPage);
      this.NavMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.NavMain.Location = new System.Drawing.Point(0, 0);
      this.NavMain.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.NavMain.Name = "NavMain";
      this.NavMain.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.customerPage,
            this.productPage,
            this.orderPage,
            this.stockPage,
            this.reportPage});
      this.NavMain.SelectedPage = this.orderPage;
      this.NavMain.Size = new System.Drawing.Size(783, 533);
      this.NavMain.TabIndex = 0;
      this.NavMain.TransitionAnimationProperties.FrameCount = 300;
      // 
      // customerPage
      // 
      this.customerPage.Controls.Add(this.txtEmail);
      this.customerPage.Controls.Add(this.txtAddress);
      this.customerPage.Controls.Add(this.labelControl5);
      this.customerPage.Controls.Add(this.txtPhone);
      this.customerPage.Controls.Add(this.labelControl4);
      this.customerPage.Controls.Add(this.txtCustomer);
      this.customerPage.Controls.Add(this.labelControl3);
      this.customerPage.Controls.Add(this.labelControl2);
      this.customerPage.Controls.Add(this.lbl);
      this.customerPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.customerPage.Name = "customerPage";
      this.customerPage.Size = new System.Drawing.Size(783, 533);
      // 
      // txtEmail
      // 
      this.txtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtEmail.Location = new System.Drawing.Point(281, 279);
      this.txtEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtEmail.Name = "txtEmail";
      this.txtEmail.Size = new System.Drawing.Size(302, 23);
      this.txtEmail.TabIndex = 4;
      // 
      // txtAddress
      // 
      this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtAddress.Location = new System.Drawing.Point(281, 235);
      this.txtAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtAddress.Name = "txtAddress";
      this.txtAddress.Size = new System.Drawing.Size(302, 23);
      this.txtAddress.TabIndex = 3;
      // 
      // labelControl5
      // 
      this.labelControl5.Location = new System.Drawing.Point(150, 283);
      this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl5.Name = "labelControl5";
      this.labelControl5.Size = new System.Drawing.Size(31, 16);
      this.labelControl5.TabIndex = 3;
      this.labelControl5.Text = "Email";
      // 
      // txtPhone
      // 
      this.txtPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtPhone.Location = new System.Drawing.Point(281, 191);
      this.txtPhone.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtPhone.Name = "txtPhone";
      this.txtPhone.Size = new System.Drawing.Size(302, 23);
      this.txtPhone.TabIndex = 2;
      // 
      // labelControl4
      // 
      this.labelControl4.Location = new System.Drawing.Point(150, 239);
      this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl4.Name = "labelControl4";
      this.labelControl4.Size = new System.Drawing.Size(40, 17);
      this.labelControl4.TabIndex = 3;
      this.labelControl4.Text = "Địa chỉ";
      // 
      // txtCustomer
      // 
      this.txtCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCustomer.Location = new System.Drawing.Point(281, 146);
      this.txtCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtCustomer.Name = "txtCustomer";
      this.txtCustomer.Size = new System.Drawing.Size(302, 23);
      this.txtCustomer.TabIndex = 1;
      // 
      // labelControl3
      // 
      this.labelControl3.Location = new System.Drawing.Point(150, 194);
      this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl3.Name = "labelControl3";
      this.labelControl3.Size = new System.Drawing.Size(61, 17);
      this.labelControl3.TabIndex = 3;
      this.labelControl3.Text = "Điện thoại";
      // 
      // labelControl2
      // 
      this.labelControl2.Location = new System.Drawing.Point(150, 150);
      this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl2.Name = "labelControl2";
      this.labelControl2.Size = new System.Drawing.Size(91, 16);
      this.labelControl2.TabIndex = 3;
      this.labelControl2.Text = "Tên khách hàng";
      // 
      // lbl
      // 
      this.lbl.Appearance.Font = new System.Drawing.Font("Tahoma", 25.25F, System.Drawing.FontStyle.Underline);
      this.lbl.Appearance.ForeColor = System.Drawing.Color.Gray;
      this.lbl.Appearance.Options.UseFont = true;
      this.lbl.Appearance.Options.UseForeColor = true;
      this.lbl.Appearance.Options.UseTextOptions = true;
      this.lbl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.lbl.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
      this.lbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
      this.lbl.Dock = System.Windows.Forms.DockStyle.Top;
      this.lbl.Location = new System.Drawing.Point(0, 0);
      this.lbl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.lbl.Name = "lbl";
      this.lbl.Size = new System.Drawing.Size(783, 105);
      this.lbl.TabIndex = 2;
      this.lbl.Text = "Nhập thông tin khách hàng";
      // 
      // productPage
      // 
      this.productPage.Controls.Add(this.cboUnit);
      this.productPage.Controls.Add(this.txtPrice);
      this.productPage.Controls.Add(this.labelControl7);
      this.productPage.Controls.Add(this.txtPaintCode);
      this.productPage.Controls.Add(this.txtPaintName);
      this.productPage.Controls.Add(this.labelControl6);
      this.productPage.Controls.Add(this.labelControl8);
      this.productPage.Controls.Add(this.labelControl9);
      this.productPage.Controls.Add(this.labelControl1);
      this.productPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.productPage.Name = "productPage";
      this.productPage.Size = new System.Drawing.Size(783, 533);
      // 
      // cboUnit
      // 
      this.cboUnit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.cboUnit.Location = new System.Drawing.Point(227, 238);
      this.cboUnit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.cboUnit.Name = "cboUnit";
      this.cboUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.cboUnit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Nước", ((short)(1)), 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dầu", ((short)(2)), 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Kẽm", ((short)(3)), 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Bột", ((short)(4)), -1)});
      this.cboUnit.Properties.SmallImages = this.imgUnit;
      this.cboUnit.Size = new System.Drawing.Size(329, 23);
      this.cboUnit.TabIndex = 13;
      // 
      // imgUnit
      // 
      this.imgUnit.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgUnit.ImageStream")));
      this.imgUnit.Images.SetKeyName(0, "fullstackedsplinearea_32x32.png");
      this.imgUnit.Images.SetKeyName(1, "database_32x32.png");
      this.imgUnit.Images.SetKeyName(2, "pictureshapefillcolor_32x32.png");
      this.imgUnit.Images.SetKeyName(3, "boproduct_16x16.png");
      this.imgUnit.InsertGalleryImage("withtextwrapping_bottomcenter_16x16.png", "images/arrange/withtextwrapping_bottomcenter_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/arrange/withtextwrapping_bottomcenter_16x16.png"), 4);
      this.imgUnit.Images.SetKeyName(4, "withtextwrapping_bottomcenter_16x16.png");
      // 
      // txtPrice
      // 
      this.txtPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtPrice.Location = new System.Drawing.Point(227, 289);
      this.txtPrice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtPrice.Name = "txtPrice";
      this.txtPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
      this.txtPrice.Size = new System.Drawing.Size(329, 23);
      this.txtPrice.TabIndex = 10;
      // 
      // labelControl7
      // 
      this.labelControl7.Location = new System.Drawing.Point(129, 290);
      this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl7.Name = "labelControl7";
      this.labelControl7.Size = new System.Drawing.Size(87, 17);
      this.labelControl7.TabIndex = 6;
      this.labelControl7.Text = "Đơn giá nhập:";
      // 
      // txtPaintCode
      // 
      this.txtPaintCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtPaintCode.Location = new System.Drawing.Point(227, 186);
      this.txtPaintCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtPaintCode.Name = "txtPaintCode";
      this.txtPaintCode.Size = new System.Drawing.Size(329, 23);
      this.txtPaintCode.TabIndex = 12;
      // 
      // txtPaintName
      // 
      this.txtPaintName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtPaintName.Location = new System.Drawing.Point(227, 134);
      this.txtPaintName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtPaintName.Name = "txtPaintName";
      this.txtPaintName.Size = new System.Drawing.Size(329, 23);
      this.txtPaintName.TabIndex = 12;
      // 
      // labelControl6
      // 
      this.labelControl6.Location = new System.Drawing.Point(129, 188);
      this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl6.Name = "labelControl6";
      this.labelControl6.Size = new System.Drawing.Size(92, 17);
      this.labelControl6.TabIndex = 8;
      this.labelControl6.Text = "Mã Sản Phẩm :";
      // 
      // labelControl8
      // 
      this.labelControl8.Location = new System.Drawing.Point(129, 239);
      this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl8.Name = "labelControl8";
      this.labelControl8.Size = new System.Drawing.Size(33, 17);
      this.labelControl8.TabIndex = 7;
      this.labelControl8.Text = "Loại :";
      // 
      // labelControl9
      // 
      this.labelControl9.Location = new System.Drawing.Point(129, 135);
      this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl9.Name = "labelControl9";
      this.labelControl9.Size = new System.Drawing.Size(98, 17);
      this.labelControl9.TabIndex = 8;
      this.labelControl9.Text = "Tên Sản Phẩm :";
      // 
      // labelControl1
      // 
      this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 25.25F, System.Drawing.FontStyle.Underline);
      this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Gray;
      this.labelControl1.Appearance.Options.UseFont = true;
      this.labelControl1.Appearance.Options.UseForeColor = true;
      this.labelControl1.Appearance.Options.UseTextOptions = true;
      this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
      this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
      this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelControl1.Location = new System.Drawing.Point(0, 0);
      this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(783, 90);
      this.labelControl1.TabIndex = 3;
      this.labelControl1.Text = "Quản lý sản phẩm Sơn";
      // 
      // orderPage
      // 
      this.orderPage.Caption = "orderPage";
      this.orderPage.Controls.Add(this.tabOrder);
      this.orderPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.orderPage.Name = "orderPage";
      this.orderPage.Size = new System.Drawing.Size(783, 533);
      // 
      // tabOrder
      // 
      this.tabOrder.Controls.Add(this.tbOrderPage);
      this.tabOrder.Controls.Add(this.tbOrderHistory);
      this.tabOrder.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabOrder.Location = new System.Drawing.Point(0, 0);
      this.tabOrder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.tabOrder.Name = "tabOrder";
      this.tabOrder.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tbOrderPage,
            this.tbOrderHistory});
      this.tabOrder.RegularSize = new System.Drawing.Size(783, 533);
      this.tabOrder.SelectedPage = this.tbOrderPage;
      this.tabOrder.Size = new System.Drawing.Size(783, 533);
      this.tabOrder.TabIndex = 5;
      this.tabOrder.Text = "OrderTabNav";
      this.tabOrder.SelectedPageChanged += new DevExpress.XtraBars.Navigation.SelectedPageChangedEventHandler(this.tabPane1_SelectedPageChanged);
      // 
      // tbOrderPage
      // 
      this.tbOrderPage.Caption = "Main";
      this.tbOrderPage.Controls.Add(this.groupControl2);
      this.tbOrderPage.Controls.Add(this.groupControl1);
      this.tbOrderPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.tbOrderPage.Name = "tbOrderPage";
      this.tbOrderPage.PageText = "Bán Hàng";
      this.tbOrderPage.Size = new System.Drawing.Size(783, 506);
      this.tbOrderPage.Tag = "MainPage";
      // 
      // groupControl2
      // 
      this.groupControl2.Controls.Add(this.gridProduct);
      this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupControl2.Location = new System.Drawing.Point(0, 210);
      this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.groupControl2.Name = "groupControl2";
      this.groupControl2.Size = new System.Drawing.Size(783, 296);
      this.groupControl2.TabIndex = 4;
      this.groupControl2.Text = "Thông tin sản phẩm ";
      // 
      // gridProduct
      // 
      this.gridProduct.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gridProduct.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      gridLevelNode2.RelationName = "Level1";
      this.gridProduct.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
      this.gridProduct.Location = new System.Drawing.Point(2, 27);
      this.gridProduct.MainView = this.gvProduct;
      this.gridProduct.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.gridProduct.Name = "gridProduct";
      this.gridProduct.Size = new System.Drawing.Size(779, 267);
      this.gridProduct.TabIndex = 1;
      this.gridProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvProduct});
      this.gridProduct.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridProduct_ProcessGridKey);
      // 
      // gvProduct
      // 
      this.gvProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            gridColumn6,
            this.gridColumn3,
            this.gridColumn2,
            this.gc15,
            this.gc17,
            this.gridColumn4,
            gridColumn5});
      this.gvProduct.DetailHeight = 431;
      this.gvProduct.GridControl = this.gridProduct;
      this.gvProduct.Name = "gvProduct";
      this.gvProduct.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
      this.gvProduct.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvProduct_CellValueChanged);
      this.gvProduct.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gvProduct_CustomUnboundColumnData);
      this.gvProduct.MouseLeave += new System.EventHandler(this.gvProduct_MouseLeave);
      // 
      // gridColumn1
      // 
      this.gridColumn1.Caption = "Tên Sản Phẩm";
      this.gridColumn1.FieldName = "Name";
      this.gridColumn1.MinWidth = 100;
      this.gridColumn1.Name = "gridColumn1";
      this.gridColumn1.OptionsColumn.AllowEdit = false;
      this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
      this.gridColumn1.OptionsColumn.ReadOnly = true;
      this.gridColumn1.Visible = true;
      this.gridColumn1.VisibleIndex = 0;
      this.gridColumn1.Width = 230;
      // 
      // gridColumn3
      // 
      this.gridColumn3.Caption = "Số Lượng";
      this.gridColumn3.FieldName = "Quantity";
      this.gridColumn3.MinWidth = 23;
      this.gridColumn3.Name = "gridColumn3";
      this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
      this.gridColumn3.Visible = true;
      this.gridColumn3.VisibleIndex = 2;
      this.gridColumn3.Width = 73;
      // 
      // gridColumn2
      // 
      this.gridColumn2.Caption = "Đơn Giá";
      this.gridColumn2.DisplayFormat.FormatString = "c";
      this.gridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
      this.gridColumn2.FieldName = "Price";
      this.gridColumn2.MinWidth = 23;
      this.gridColumn2.Name = "gridColumn2";
      this.gridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
      this.gridColumn2.Visible = true;
      this.gridColumn2.VisibleIndex = 3;
      this.gridColumn2.Width = 121;
      // 
      // gc15
      // 
      this.gc15.AppearanceHeader.Options.UseTextOptions = true;
      this.gc15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.gc15.Caption = "15%";
      this.gc15.FieldName = "gc15";
      this.gc15.MinWidth = 24;
      this.gc15.Name = "gc15";
      this.gc15.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
      this.gc15.OptionsColumn.FixedWidth = true;
      this.gc15.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
      this.gc15.Visible = true;
      this.gc15.VisibleIndex = 4;
      this.gc15.Width = 50;
      // 
      // gc17
      // 
      this.gc17.AppearanceHeader.Options.UseTextOptions = true;
      this.gc17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.gc17.Caption = "17%";
      this.gc17.FieldName = "gc17";
      this.gc17.MinWidth = 24;
      this.gc17.Name = "gc17";
      this.gc17.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
      this.gc17.OptionsColumn.FixedWidth = true;
      this.gc17.UnboundExpression = "[gc15] = False";
      this.gc17.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
      this.gc17.Visible = true;
      this.gc17.VisibleIndex = 5;
      this.gc17.Width = 50;
      // 
      // gridColumn4
      // 
      this.gridColumn4.AppearanceCell.Options.UseTextOptions = true;
      this.gridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
      this.gridColumn4.Caption = "Thành Tiền";
      this.gridColumn4.DisplayFormat.FormatString = "c";
      this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
      this.gridColumn4.FieldName = "Total";
      this.gridColumn4.MinWidth = 23;
      this.gridColumn4.Name = "gridColumn4";
      this.gridColumn4.OptionsColumn.AllowEdit = false;
      this.gridColumn4.OptionsColumn.AllowMove = false;
      this.gridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
      this.gridColumn4.OptionsColumn.ReadOnly = true;
      this.gridColumn4.UnboundExpression = "[Quantity] * [Price]";
      this.gridColumn4.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
      this.gridColumn4.Visible = true;
      this.gridColumn4.VisibleIndex = 6;
      this.gridColumn4.Width = 139;
      // 
      // groupControl1
      // 
      this.groupControl1.Controls.Add(this.pnDocumentManager);
      this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
      this.groupControl1.Location = new System.Drawing.Point(0, 0);
      this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.groupControl1.Name = "groupControl1";
      this.groupControl1.Size = new System.Drawing.Size(783, 210);
      this.groupControl1.TabIndex = 3;
      this.groupControl1.Text = "Chọn thông tin khách hàng";
      // 
      // pnDocumentManager
      // 
      this.pnDocumentManager.Controls.Add(this.DateOrder);
      this.pnDocumentManager.Controls.Add(this.cboCustomer);
      this.pnDocumentManager.Controls.Add(this.btnReport);
      this.pnDocumentManager.Controls.Add(this.labelControl14);
      this.pnDocumentManager.Controls.Add(this.txtNote);
      this.pnDocumentManager.Controls.Add(this.labelControl19);
      this.pnDocumentManager.Controls.Add(this.labelControl10);
      this.pnDocumentManager.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnDocumentManager.Location = new System.Drawing.Point(2, 27);
      this.pnDocumentManager.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.pnDocumentManager.Name = "pnDocumentManager";
      this.pnDocumentManager.Size = new System.Drawing.Size(779, 181);
      this.pnDocumentManager.TabIndex = 0;
      // 
      // DateOrder
      // 
      this.DateOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.DateOrder.EditValue = new System.DateTime(2019, 8, 23, 14, 45, 58, 528);
      this.DateOrder.EnterMoveNextControl = true;
      this.DateOrder.Location = new System.Drawing.Point(194, 17);
      this.DateOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
      this.DateOrder.Name = "DateOrder";
      this.DateOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.DateOrder.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.DateOrder.Size = new System.Drawing.Size(432, 23);
      this.DateOrder.TabIndex = 8;
      // 
      // cboCustomer
      // 
      this.cboCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.cboCustomer.EditValue = "csa";
      this.cboCustomer.Location = new System.Drawing.Point(194, 50);
      this.cboCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.cboCustomer.Name = "cboCustomer";
      this.cboCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.cboCustomer.Properties.DisplayMember = "Name";
      this.cboCustomer.Properties.NullText = "Chọn Khách Hàng";
      this.cboCustomer.Properties.NullValuePrompt = "0";
      this.cboCustomer.Properties.PopupView = this.searchLookUpEdit1View;
      this.cboCustomer.Properties.ValueMember = "ID";
      this.cboCustomer.Size = new System.Drawing.Size(432, 23);
      this.cboCustomer.TabIndex = 7;
      // 
      // searchLookUpEdit1View
      // 
      this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCustomerName,
            this.colCustomerPhone,
            this.colCustomerAddress});
      this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
      this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
      this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
      this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
      // 
      // colCustomerName
      // 
      this.colCustomerName.Caption = "Tên Khách Hàng";
      this.colCustomerName.FieldName = "Name";
      this.colCustomerName.Name = "colCustomerName";
      this.colCustomerName.Visible = true;
      this.colCustomerName.VisibleIndex = 0;
      // 
      // colCustomerPhone
      // 
      this.colCustomerPhone.Caption = "Điện Thoại";
      this.colCustomerPhone.FieldName = "Phone";
      this.colCustomerPhone.Name = "colCustomerPhone";
      this.colCustomerPhone.Visible = true;
      this.colCustomerPhone.VisibleIndex = 1;
      // 
      // colCustomerAddress
      // 
      this.colCustomerAddress.Caption = "Địa Chỉ";
      this.colCustomerAddress.FieldName = "Address";
      this.colCustomerAddress.Name = "colCustomerAddress";
      this.colCustomerAddress.Visible = true;
      this.colCustomerAddress.VisibleIndex = 2;
      // 
      // btnReport
      // 
      this.btnReport.Location = new System.Drawing.Point(315, 142);
      this.btnReport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.btnReport.Name = "btnReport";
      this.btnReport.Size = new System.Drawing.Size(152, 28);
      this.btnReport.TabIndex = 6;
      this.btnReport.Text = "In báo cáo";
      this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
      // 
      // labelControl14
      // 
      this.labelControl14.Location = new System.Drawing.Point(112, 98);
      this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl14.Name = "labelControl14";
      this.labelControl14.Size = new System.Drawing.Size(44, 16);
      this.labelControl14.TabIndex = 3;
      this.labelControl14.Text = "Ghi Chú";
      // 
      // txtNote
      // 
      this.txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtNote.Location = new System.Drawing.Point(194, 85);
      this.txtNote.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtNote.Multiline = true;
      this.txtNote.Name = "txtNote";
      this.txtNote.Size = new System.Drawing.Size(432, 48);
      this.txtNote.TabIndex = 2;
      // 
      // labelControl19
      // 
      this.labelControl19.Location = new System.Drawing.Point(103, 20);
      this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl19.Name = "labelControl19";
      this.labelControl19.Size = new System.Drawing.Size(53, 16);
      this.labelControl19.TabIndex = 1;
      this.labelControl19.Text = "Ngày Bán";
      // 
      // labelControl10
      // 
      this.labelControl10.Location = new System.Drawing.Point(89, 54);
      this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl10.Name = "labelControl10";
      this.labelControl10.Size = new System.Drawing.Size(67, 16);
      this.labelControl10.TabIndex = 1;
      this.labelControl10.Text = "Khách Hàng";
      // 
      // tbOrderHistory
      // 
      this.tbOrderHistory.Caption = "History";
      this.tbOrderHistory.Controls.Add(this.ucOrderHistory1);
      this.tbOrderHistory.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.tbOrderHistory.Name = "tbOrderHistory";
      this.tbOrderHistory.PageText = "Lịch Sử Bán Hàng";
      this.tbOrderHistory.Size = new System.Drawing.Size(783, 506);
      this.tbOrderHistory.Tag = "History";
      // 
      // ucOrderHistory1
      // 
      this.ucOrderHistory1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ucOrderHistory1.Location = new System.Drawing.Point(0, 0);
      this.ucOrderHistory1.Margin = new System.Windows.Forms.Padding(5);
      this.ucOrderHistory1.Name = "ucOrderHistory1";
      this.ucOrderHistory1.OrderId = 0;
      this.ucOrderHistory1.Size = new System.Drawing.Size(783, 506);
      this.ucOrderHistory1.TabIndex = 0;
      // 
      // stockPage
      // 
      this.stockPage.Caption = "stockPage";
      this.stockPage.Controls.Add(this.groupControl4);
      this.stockPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.stockPage.Name = "stockPage";
      this.stockPage.Size = new System.Drawing.Size(783, 533);
      // 
      // groupControl4
      // 
      this.groupControl4.Controls.Add(this.labelControl17);
      this.groupControl4.Controls.Add(this.labelControl16);
      this.groupControl4.Controls.Add(this.dtImport);
      this.groupControl4.Controls.Add(this.txtStockCode);
      this.groupControl4.Controls.Add(this.txtStockNote);
      this.groupControl4.Controls.Add(this.txtPriceImport);
      this.groupControl4.Controls.Add(this.labelControl20);
      this.groupControl4.Controls.Add(this.labelControl18);
      this.groupControl4.Controls.Add(this.txtQuantity);
      this.groupControl4.Controls.Add(this.labelControl15);
      this.groupControl4.Controls.Add(this.labelControl13);
      this.groupControl4.Controls.Add(this.labelControl11);
      this.groupControl4.Controls.Add(this.cboProduct);
      this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupControl4.Location = new System.Drawing.Point(0, 0);
      this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.groupControl4.Name = "groupControl4";
      this.groupControl4.Size = new System.Drawing.Size(783, 533);
      this.groupControl4.TabIndex = 0;
      this.groupControl4.Text = "Nhập Sản Phẩm";
      // 
      // labelControl17
      // 
      this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 25.25F, System.Drawing.FontStyle.Underline);
      this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Gray;
      this.labelControl17.Appearance.Options.UseFont = true;
      this.labelControl17.Appearance.Options.UseForeColor = true;
      this.labelControl17.Appearance.Options.UseTextOptions = true;
      this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.labelControl17.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
      this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
      this.labelControl17.Dock = System.Windows.Forms.DockStyle.Top;
      this.labelControl17.Location = new System.Drawing.Point(2, 27);
      this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl17.Name = "labelControl17";
      this.labelControl17.Size = new System.Drawing.Size(779, 90);
      this.labelControl17.TabIndex = 6;
      this.labelControl17.Text = "Quản lý nhập kho";
      // 
      // labelControl16
      // 
      this.labelControl16.Location = new System.Drawing.Point(91, 322);
      this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl16.Name = "labelControl16";
      this.labelControl16.Size = new System.Drawing.Size(68, 17);
      this.labelControl16.TabIndex = 5;
      this.labelControl16.Text = "Ngày Nhập";
      // 
      // dtImport
      // 
      this.dtImport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dtImport.EditValue = null;
      this.dtImport.Location = new System.Drawing.Point(234, 319);
      this.dtImport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.dtImport.Name = "dtImport";
      this.dtImport.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.dtImport.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.dtImport.Size = new System.Drawing.Size(404, 23);
      this.dtImport.TabIndex = 4;
      // 
      // txtStockCode
      // 
      this.txtStockCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtStockCode.Location = new System.Drawing.Point(234, 205);
      this.txtStockCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtStockCode.Name = "txtStockCode";
      this.txtStockCode.Size = new System.Drawing.Size(404, 23);
      this.txtStockCode.TabIndex = 3;
      // 
      // txtStockNote
      // 
      this.txtStockNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtStockNote.Location = new System.Drawing.Point(234, 357);
      this.txtStockNote.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtStockNote.Name = "txtStockNote";
      this.txtStockNote.Size = new System.Drawing.Size(404, 23);
      this.txtStockNote.TabIndex = 3;
      // 
      // txtPriceImport
      // 
      this.txtPriceImport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtPriceImport.Location = new System.Drawing.Point(234, 281);
      this.txtPriceImport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtPriceImport.Name = "txtPriceImport";
      this.txtPriceImport.Properties.Mask.EditMask = "#,###,###,###";
      this.txtPriceImport.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
      this.txtPriceImport.Size = new System.Drawing.Size(404, 23);
      this.txtPriceImport.TabIndex = 3;
      // 
      // labelControl20
      // 
      this.labelControl20.Location = new System.Drawing.Point(92, 208);
      this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl20.Name = "labelControl20";
      this.labelControl20.Size = new System.Drawing.Size(83, 17);
      this.labelControl20.TabIndex = 2;
      this.labelControl20.Text = "Mã Sản Phẩm";
      // 
      // labelControl18
      // 
      this.labelControl18.Location = new System.Drawing.Point(92, 360);
      this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl18.Name = "labelControl18";
      this.labelControl18.Size = new System.Drawing.Size(44, 16);
      this.labelControl18.TabIndex = 2;
      this.labelControl18.Text = "Ghi Chú";
      // 
      // txtQuantity
      // 
      this.txtQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtQuantity.Location = new System.Drawing.Point(234, 243);
      this.txtQuantity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtQuantity.Name = "txtQuantity";
      this.txtQuantity.Size = new System.Drawing.Size(404, 23);
      this.txtQuantity.TabIndex = 3;
      // 
      // labelControl15
      // 
      this.labelControl15.Location = new System.Drawing.Point(91, 284);
      this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl15.Name = "labelControl15";
      this.labelControl15.Size = new System.Drawing.Size(54, 17);
      this.labelControl15.TabIndex = 2;
      this.labelControl15.Text = "Giá Nhập";
      // 
      // labelControl13
      // 
      this.labelControl13.Location = new System.Drawing.Point(92, 246);
      this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl13.Name = "labelControl13";
      this.labelControl13.Size = new System.Drawing.Size(60, 17);
      this.labelControl13.TabIndex = 2;
      this.labelControl13.Text = "Số Lượng";
      // 
      // labelControl11
      // 
      this.labelControl11.Location = new System.Drawing.Point(92, 170);
      this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl11.Name = "labelControl11";
      this.labelControl11.Size = new System.Drawing.Size(99, 17);
      this.labelControl11.TabIndex = 1;
      this.labelControl11.Text = "Chọn Sản Phẩm";
      // 
      // cboProduct
      // 
      this.cboProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.cboProduct.Location = new System.Drawing.Point(233, 167);
      this.cboProduct.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.cboProduct.Name = "cboProduct";
      this.cboProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.cboProduct.Properties.DisplayMember = "Name";
      this.cboProduct.Properties.NullText = "Chọn sản phẩm";
      this.cboProduct.Properties.PopupView = this.gridView2;
      this.cboProduct.Properties.ValueMember = "ID";
      this.cboProduct.Size = new System.Drawing.Size(405, 23);
      this.cboProduct.TabIndex = 0;
      // 
      // gridView2
      // 
      this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
      this.gridView2.Name = "gridView2";
      this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
      this.gridView2.OptionsView.ShowGroupPanel = false;
      // 
      // reportPage
      // 
      this.reportPage.Caption = "reportPage";
      this.reportPage.Controls.Add(this.groupControl5);
      this.reportPage.Controls.Add(this.groupControl6);
      this.reportPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.reportPage.Name = "reportPage";
      this.reportPage.Size = new System.Drawing.Size(783, 533);
      // 
      // groupControl5
      // 
      this.groupControl5.Controls.Add(this.grStock);
      this.groupControl5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupControl5.Location = new System.Drawing.Point(0, 55);
      this.groupControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.groupControl5.Name = "groupControl5";
      this.groupControl5.Size = new System.Drawing.Size(783, 478);
      this.groupControl5.TabIndex = 0;
      this.groupControl5.Text = "Thông Tin Tồn Kho";
      // 
      // grStock
      // 
      this.grStock.Dock = System.Windows.Forms.DockStyle.Fill;
      this.grStock.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.grStock.Location = new System.Drawing.Point(2, 27);
      this.grStock.MainView = this.gvReport;
      this.grStock.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.grStock.Name = "grStock";
      this.grStock.Size = new System.Drawing.Size(779, 449);
      this.grStock.TabIndex = 8;
      this.grStock.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
      // 
      // gvReport
      // 
      this.gvReport.DetailHeight = 431;
      this.gvReport.GridControl = this.grStock;
      this.gvReport.Name = "gvReport";
      this.gvReport.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
      this.gvReport.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
      this.gvReport.OptionsBehavior.Editable = false;
      this.gvReport.OptionsSelection.MultiSelect = true;
      // 
      // groupControl6
      // 
      this.groupControl6.Controls.Add(this.button1);
      this.groupControl6.Controls.Add(this.txtSearchBaoCao);
      this.groupControl6.Controls.Add(this.labelControl21);
      this.groupControl6.Dock = System.Windows.Forms.DockStyle.Top;
      this.groupControl6.Location = new System.Drawing.Point(0, 0);
      this.groupControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.groupControl6.Name = "groupControl6";
      this.groupControl6.Size = new System.Drawing.Size(783, 55);
      this.groupControl6.TabIndex = 10;
      this.groupControl6.Text = "Tìm kiếm thông tin sản phẩm";
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(660, 29);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(111, 23);
      this.button1.TabIndex = 2;
      this.button1.Text = "In Báo Cáo";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // txtSearchBaoCao
      // 
      this.txtSearchBaoCao.Dock = System.Windows.Forms.DockStyle.Left;
      this.txtSearchBaoCao.Location = new System.Drawing.Point(24, 27);
      this.txtSearchBaoCao.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtSearchBaoCao.Name = "txtSearchBaoCao";
      this.txtSearchBaoCao.Size = new System.Drawing.Size(612, 23);
      this.txtSearchBaoCao.TabIndex = 0;
      this.txtSearchBaoCao.EditValueChanged += new System.EventHandler(this.txtSearchBaoCao_EditValueChanged);
      // 
      // labelControl21
      // 
      this.labelControl21.Dock = System.Windows.Forms.DockStyle.Left;
      this.labelControl21.Location = new System.Drawing.Point(2, 27);
      this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl21.Name = "labelControl21";
      this.labelControl21.Size = new System.Drawing.Size(22, 16);
      this.labelControl21.TabIndex = 1;
      this.labelControl21.Text = "Tìm";
      // 
      // panelControl1
      // 
      this.panelControl1.Controls.Add(this.hyperlinkLabelControl1);
      this.panelControl1.Controls.Add(this.btnpnNavigation);
      this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelControl1.Location = new System.Drawing.Point(0, 0);
      this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.panelControl1.Name = "panelControl1";
      this.panelControl1.Size = new System.Drawing.Size(1390, 126);
      this.panelControl1.TabIndex = 3;
      // 
      // hyperlinkLabelControl1
      // 
      this.hyperlinkLabelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI Light", 36F);
      this.hyperlinkLabelControl1.Appearance.Options.UseFont = true;
      this.hyperlinkLabelControl1.LineStyle = System.Drawing.Drawing2D.DashStyle.Custom;
      this.hyperlinkLabelControl1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
      this.hyperlinkLabelControl1.Location = new System.Drawing.Point(28, 21);
      this.hyperlinkLabelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.hyperlinkLabelControl1.Name = "hyperlinkLabelControl1";
      this.hyperlinkLabelControl1.Size = new System.Drawing.Size(326, 81);
      this.hyperlinkLabelControl1.TabIndex = 4;
      this.hyperlinkLabelControl1.Text = "Đồ Gia Dụng";
      // 
      // btnpnNavigation
      // 
      this.btnpnNavigation.AllowGlyphSkinning = false;
      this.btnpnNavigation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnpnNavigation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      windowsUIButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions9.Image")));
      windowsUIButtonImageOptions9.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
      windowsUIButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions10.Image")));
      windowsUIButtonImageOptions10.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
      windowsUIButtonImageOptions11.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions11.Image")));
      windowsUIButtonImageOptions11.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
      windowsUIButtonImageOptions12.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions12.Image")));
      windowsUIButtonImageOptions12.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
      windowsUIButtonImageOptions13.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions13.Image")));
      windowsUIButtonImageOptions13.Location = DevExpress.XtraBars.Docking2010.ImageLocation.BeforeText;
      this.btnpnNavigation.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Khách Hàng", true, windowsUIButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Customer", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Sản Phẩm", true, windowsUIButtonImageOptions10, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Product", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Đơn Hàng", true, windowsUIButtonImageOptions11, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Order", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Kho", true, windowsUIButtonImageOptions12, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Stock", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Báo Cáo", true, windowsUIButtonImageOptions13, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Report", -1, false)});
      this.btnpnNavigation.ForeColor = System.Drawing.Color.MidnightBlue;
      this.btnpnNavigation.Location = new System.Drawing.Point(797, 15);
      this.btnpnNavigation.Margin = new System.Windows.Forms.Padding(17, 4, 3, 4);
      this.btnpnNavigation.Name = "btnpnNavigation";
      this.btnpnNavigation.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.btnpnNavigation.Size = new System.Drawing.Size(579, 97);
      this.btnpnNavigation.TabIndex = 5;
      this.btnpnNavigation.Text = "windowsUIButtonPanel1";
      this.btnpnNavigation.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanel1_ButtonClick);
      // 
      // panelControl2
      // 
      this.panelControl2.Controls.Add(this.splitContainerControl1);
      this.panelControl2.Controls.Add(this.panelControl4);
      this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelControl2.Location = new System.Drawing.Point(0, 126);
      this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.panelControl2.Name = "panelControl2";
      this.panelControl2.Size = new System.Drawing.Size(1390, 653);
      this.panelControl2.TabIndex = 4;
      // 
      // splitContainerControl1
      // 
      this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainerControl1.Location = new System.Drawing.Point(2, 2);
      this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.splitContainerControl1.Name = "splitContainerControl1";
      this.splitContainerControl1.Panel1.Controls.Add(this.gbInfo);
      this.splitContainerControl1.Panel1.Controls.Add(this.groupControl3);
      this.splitContainerControl1.Panel1.Text = "Panel1";
      this.splitContainerControl1.Panel2.Controls.Add(this.NavMain);
      this.splitContainerControl1.Panel2.Text = "Panel2";
      this.splitContainerControl1.Size = new System.Drawing.Size(1386, 533);
      this.splitContainerControl1.SplitterPosition = 598;
      this.splitContainerControl1.TabIndex = 2;
      // 
      // gbInfo
      // 
      this.gbInfo.Controls.Add(this.MainGrid);
      this.gbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbInfo.Location = new System.Drawing.Point(0, 55);
      this.gbInfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.gbInfo.Name = "gbInfo";
      this.gbInfo.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.gbInfo.Size = new System.Drawing.Size(598, 478);
      this.gbInfo.TabIndex = 8;
      this.gbInfo.TabStop = false;
      this.gbInfo.Text = "Thông tin : ";
      // 
      // MainGrid
      // 
      this.MainGrid.DataSource = this.mainBindingSource;
      this.MainGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.MainGrid.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.MainGrid.Location = new System.Drawing.Point(3, 20);
      this.MainGrid.MainView = this.gridView1;
      this.MainGrid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.MainGrid.Name = "MainGrid";
      this.MainGrid.Size = new System.Drawing.Size(592, 454);
      this.MainGrid.TabIndex = 7;
      this.MainGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
      // 
      // mainBindingSource
      // 
      this.mainBindingSource.DataSource = typeof(test1.Customer);
      // 
      // gridView1
      // 
      this.gridView1.DetailHeight = 431;
      this.gridView1.GridControl = this.MainGrid;
      this.gridView1.Name = "gridView1";
      this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
      this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
      this.gridView1.OptionsBehavior.Editable = false;
      this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
      this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
      // 
      // groupControl3
      // 
      this.groupControl3.Controls.Add(this.txtSearch);
      this.groupControl3.Controls.Add(this.labelControl12);
      this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
      this.groupControl3.Location = new System.Drawing.Point(0, 0);
      this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.groupControl3.Name = "groupControl3";
      this.groupControl3.Size = new System.Drawing.Size(598, 55);
      this.groupControl3.TabIndex = 9;
      this.groupControl3.Text = "Tìm kiếm thông tin";
      // 
      // txtSearch
      // 
      this.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtSearch.Location = new System.Drawing.Point(24, 27);
      this.txtSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.txtSearch.Name = "txtSearch";
      this.txtSearch.Size = new System.Drawing.Size(572, 23);
      this.txtSearch.TabIndex = 0;
      this.txtSearch.EditValueChanged += new System.EventHandler(this.txtSearch_EditValueChanged);
      // 
      // labelControl12
      // 
      this.labelControl12.Dock = System.Windows.Forms.DockStyle.Left;
      this.labelControl12.Location = new System.Drawing.Point(2, 27);
      this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.labelControl12.Name = "labelControl12";
      this.labelControl12.Size = new System.Drawing.Size(22, 16);
      this.labelControl12.TabIndex = 1;
      this.labelControl12.Text = "Tìm";
      // 
      // panelControl4
      // 
      this.panelControl4.Controls.Add(this.btnPnAction);
      this.panelControl4.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panelControl4.Location = new System.Drawing.Point(2, 535);
      this.panelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.panelControl4.Name = "panelControl4";
      this.panelControl4.Size = new System.Drawing.Size(1386, 116);
      this.panelControl4.TabIndex = 1;
      // 
      // btnPnAction
      // 
      this.btnPnAction.AllowGlyphSkinning = false;
      this.btnPnAction.AppearanceButton.Hovered.Image = ((System.Drawing.Image)(resources.GetObject("btnPnAction.AppearanceButton.Hovered.Image")));
      this.btnPnAction.AppearanceButton.Hovered.Options.UseImage = true;
      this.btnPnAction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      windowsUIButtonImageOptions14.Glyphs = this.imgAdd;
      windowsUIButtonImageOptions14.ImageIndex = 0;
      windowsUIButtonImageOptions15.EnableTransparency = true;
      windowsUIButtonImageOptions15.Glyphs = this.imgSave;
      windowsUIButtonImageOptions15.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions15.Image")));
      windowsUIButtonImageOptions15.ImageIndex = 2;
      windowsUIButtonImageOptions16.Glyphs = this.imgDelete;
      windowsUIButtonImageOptions16.Image = ((System.Drawing.Image)(resources.GetObject("windowsUIButtonImageOptions16.Image")));
      this.btnPnAction.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Thêm", true, windowsUIButtonImageOptions14, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Add", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Cập nhật", true, windowsUIButtonImageOptions15, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Save", -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Xóa", true, windowsUIButtonImageOptions16, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, "Delete", -1, false)});
      this.btnPnAction.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnPnAction.EnableImageTransparency = true;
      this.btnPnAction.Font = new System.Drawing.Font("Tahoma", 8.25F);
      this.btnPnAction.ForeColor = System.Drawing.Color.Transparent;
      this.btnPnAction.Images = this.imgAdd;
      this.btnPnAction.Location = new System.Drawing.Point(2, 2);
      this.btnPnAction.Margin = new System.Windows.Forms.Padding(175, 185, 175, 185);
      this.btnPnAction.Name = "btnPnAction";
      this.btnPnAction.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.btnPnAction.Size = new System.Drawing.Size(1382, 112);
      this.btnPnAction.TabIndex = 5;
      this.btnPnAction.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.btnPnAction_ButtonClick);
      // 
      // alertControl1
      // 
      this.alertControl1.AppearanceCaption.Image = ((System.Drawing.Image)(resources.GetObject("alertControl1.AppearanceCaption.Image")));
      this.alertControl1.AppearanceCaption.Options.UseImage = true;
      this.alertControl1.AppearanceHotTrackedText.Image = ((System.Drawing.Image)(resources.GetObject("alertControl1.AppearanceHotTrackedText.Image")));
      this.alertControl1.AppearanceHotTrackedText.Options.UseImage = true;
      this.alertControl1.AppearanceText.Image = ((System.Drawing.Image)(resources.GetObject("alertControl1.AppearanceText.Image")));
      this.alertControl1.AppearanceText.Options.UseImage = true;
      this.alertControl1.AutoFormDelay = 3000;
      this.alertControl1.AutoHeight = true;
      this.alertControl1.FormDisplaySpeed = DevExpress.XtraBars.Alerter.AlertFormDisplaySpeed.Fast;
      this.alertControl1.ShowPinButton = false;
      // 
      // imgGallery
      // 
      this.imgGallery.ImageSize = new System.Drawing.Size(64, 64);
      this.imgGallery.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgGallery.ImageStream")));
      this.imgGallery.Images.SetKeyName(0, "boemployee_32x32.png");
      this.imgGallery.Images.SetKeyName(1, "bocustomer_32x32.png");
      this.imgGallery.Images.SetKeyName(2, "boperson_32x32.png");
      // 
      // MainForm
      // 
      this.Appearance.BackColor = System.Drawing.Color.White;
      this.Appearance.Options.UseBackColor = true;
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1390, 779);
      this.Controls.Add(this.panelControl2);
      this.Controls.Add(this.panelControl1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.Load += new System.EventHandler(this.MainForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.imgAdd)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgSave)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgDelete)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.NavMain)).EndInit();
      this.NavMain.ResumeLayout(false);
      this.customerPage.ResumeLayout(false);
      this.customerPage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtCustomer.Properties)).EndInit();
      this.productPage.ResumeLayout(false);
      this.productPage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.cboUnit.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgUnit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPrice.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPaintCode.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPaintName.Properties)).EndInit();
      this.orderPage.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.tabOrder)).EndInit();
      this.tabOrder.ResumeLayout(false);
      this.tbOrderPage.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
      this.groupControl2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.gridProduct)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvProduct)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
      this.groupControl1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pnDocumentManager)).EndInit();
      this.pnDocumentManager.ResumeLayout(false);
      this.pnDocumentManager.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.DateOrder.Properties.CalendarTimeProperties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.DateOrder.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cboCustomer.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
      this.tbOrderHistory.ResumeLayout(false);
      this.stockPage.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
      this.groupControl4.ResumeLayout(false);
      this.groupControl4.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dtImport.Properties.CalendarTimeProperties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dtImport.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStockCode.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStockNote.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtPriceImport.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtQuantity.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.cboProduct.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
      this.reportPage.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
      this.groupControl5.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.grStock)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
      this.groupControl6.ResumeLayout(false);
      this.groupControl6.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtSearchBaoCao.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
      this.panelControl1.ResumeLayout(false);
      this.panelControl1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
      this.panelControl2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
      this.splitContainerControl1.ResumeLayout(false);
      this.gbInfo.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.MainGrid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.mainBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
      this.groupControl3.ResumeLayout(false);
      this.groupControl3.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
      this.panelControl4.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.styleController1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.imgGallery)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion
    private DevExpress.XtraBars.Navigation.NavigationFrame NavMain;
    private DevExpress.XtraBars.Navigation.NavigationPage customerPage;
    private DevExpress.XtraBars.Navigation.NavigationPage productPage;
    private DevExpress.XtraEditors.PanelControl panelControl1;
    private DevExpress.XtraEditors.HyperlinkLabelControl hyperlinkLabelControl1;
    private DevExpress.XtraEditors.PanelControl panelControl2;
    protected DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel btnpnNavigation;
    private DevExpress.XtraEditors.LabelControl lbl;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.PanelControl panelControl4;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
    private DevExpress.XtraEditors.TextEdit txtAddress;
    private DevExpress.XtraEditors.TextEdit txtPhone;
    private DevExpress.XtraEditors.LabelControl labelControl4;
    private DevExpress.XtraEditors.TextEdit txtCustomer;
    private DevExpress.XtraEditors.LabelControl labelControl3;
    private DevExpress.XtraEditors.LabelControl labelControl2;
    private DevExpress.XtraEditors.TextEdit txtEmail;
    private DevExpress.XtraEditors.LabelControl labelControl5;
    private DevExpress.XtraEditors.TextEdit txtPrice;
    private DevExpress.XtraEditors.LabelControl labelControl7;
    private DevExpress.XtraEditors.TextEdit txtPaintName;
    private DevExpress.XtraEditors.LabelControl labelControl8;
    private DevExpress.XtraEditors.LabelControl labelControl9;
    private DevExpress.XtraEditors.TextEdit txtPaintCode;
    private DevExpress.XtraEditors.LabelControl labelControl6;
    public DevExpress.Utils.ImageCollection imgAdd;
    private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel btnPnAction;
    private DevExpress.Utils.ImageCollection imgSave;
    private DevExpress.Utils.ImageCollection imgDelete;
    private DevExpress.XtraGrid.GridControl MainGrid;
    private System.Windows.Forms.BindingSource mainBindingSource;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    private DevExpress.XtraBars.Alerter.AlertControl alertControl1;
    private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
    private DevExpress.Utils.ImageCollection imgUnit;
    private DevExpress.XtraEditors.ImageComboBoxEdit cboUnit;
    private DevExpress.XtraBars.Navigation.NavigationPage orderPage;
    private DevExpress.XtraEditors.GroupControl groupControl2;
    private DevExpress.XtraGrid.GridControl gridProduct;
    private DevExpress.XtraGrid.Views.Grid.GridView gvProduct;
    private DevExpress.XtraEditors.GroupControl groupControl1;
    private System.Windows.Forms.GroupBox gbInfo;
    private DevExpress.XtraEditors.GroupControl groupControl3;
    private DevExpress.XtraEditors.TextEdit txtSearch;
    private DevExpress.XtraEditors.LabelControl labelControl12;
    public DevExpress.Utils.ImageCollection imgGallery;
    private DevExpress.XtraEditors.StyleController styleController1;
    private DevExpress.XtraEditors.PanelControl pnDocumentManager;
    private DevExpress.XtraEditors.LabelControl labelControl10;
    private System.Windows.Forms.TextBox txtNote;
    private DevExpress.XtraEditors.LabelControl labelControl14;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
    private DevExpress.XtraBars.Navigation.NavigationPage stockPage;
    private DevExpress.XtraEditors.GroupControl groupControl4;
    private DevExpress.XtraEditors.LabelControl labelControl11;
    private DevExpress.XtraEditors.SearchLookUpEdit cboProduct;
    private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    private DevExpress.XtraEditors.TextEdit txtQuantity;
    private DevExpress.XtraEditors.LabelControl labelControl13;
    private DevExpress.XtraEditors.LabelControl labelControl16;
    private DevExpress.XtraEditors.DateEdit dtImport;
    private DevExpress.XtraEditors.TextEdit txtPriceImport;
    private DevExpress.XtraEditors.LabelControl labelControl15;
    private DevExpress.XtraEditors.LabelControl labelControl17;
    private DevExpress.XtraEditors.TextEdit txtStockNote;
    private DevExpress.XtraEditors.LabelControl labelControl18;
    private DevExpress.XtraBars.Navigation.NavigationPage reportPage;
    private DevExpress.XtraEditors.GroupControl groupControl5;
    private DevExpress.XtraGrid.GridControl grStock;
    private DevExpress.XtraGrid.Views.Grid.GridView gvReport;
    private DevExpress.XtraEditors.SimpleButton btnReport;
    private DevExpress.XtraBars.Navigation.TabPane tabOrder;
    private DevExpress.XtraBars.Navigation.TabNavigationPage tbOrderPage;
    private DevExpress.XtraBars.Navigation.TabNavigationPage tbOrderHistory;
    private DevExpress.XtraEditors.SearchLookUpEdit cboCustomer;
    private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
    private DevExpress.XtraGrid.Columns.GridColumn colCustomerName;
    private DevExpress.XtraGrid.Columns.GridColumn colCustomerPhone;
    private DevExpress.XtraGrid.Columns.GridColumn colCustomerAddress;
    private ucOrderHistory ucOrderHistory1;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.DateEdit DateOrder;
    private DevExpress.XtraGrid.Columns.GridColumn gc15;
    private DevExpress.XtraGrid.Columns.GridColumn gc17;
        private DevExpress.XtraEditors.TextEdit txtStockCode;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.TextEdit txtSearchBaoCao;
        private DevExpress.XtraEditors.LabelControl labelControl21;
    private System.Windows.Forms.Button button1;
  }
}