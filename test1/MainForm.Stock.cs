﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using test1.ViewModels;

namespace test1
{
  public partial class MainForm
  {

    private void LoadControlStock()
    {
      LoadComboxProduct();
      LoadStockGrid();
      this.dtImport.EditValue = DateTime.Now;
    }
    private void LoadComboxProduct()
    {
      var products = this.stockService.GetAllProducts();
      var dataGrids = mapper.Map<List<ProductGridView>>(products);
      this.cboProduct.Properties.DataSource = dataGrids;
    }

    private void BindingStockControl(StockViewModel stockViewModel)
    {
      this.txtQuantity.DataBindings.Clear();
      this.txtQuantity.DataBindings.Add("Text", stockViewModel, "Quantity", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtStockCode.DataBindings.Clear();
      this.txtStockCode.DataBindings.Add("Text", stockViewModel, "Code", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtPriceImport.DataBindings.Clear();
      this.txtPriceImport.DataBindings.Add("Text", stockViewModel, "PriceImport", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtStockNote.DataBindings.Clear();
      this.txtStockNote.DataBindings.Add("Text", stockViewModel, "Note", false, DataSourceUpdateMode.OnPropertyChanged);

      this.dtImport.DataBindings.Clear();
      this.dtImport.DataBindings.Add("EditValue", stockViewModel, "CreatedDate", false, DataSourceUpdateMode.OnPropertyChanged);

      this.cboProduct.DataBindings.Clear();
      this.cboProduct.DataBindings.Add("EditValue", stockViewModel, "ProductId", false, DataSourceUpdateMode.OnPropertyChanged);

    }

    private void LoadStockGrid()
    {
      this.gridView1.Columns.Clear();
      var stocks = stockService.GetStocks();

      var dataGrids = mapper.Map<List<StockGridView>>(stocks);
      mainBindingSource.DataSource = dataGrids;
    }
 
    private void AddOrUpdateStock(StockViewModel stockViewModel)
    {
      if (!Validation(stockViewModel)) return;

      var data = mapper.Map<Stock>(stockViewModel);
      data.Year = (short)stockViewModel.CreatedDate.Year;
      data.Month = (short)stockViewModel.CreatedDate.Month;

      var stock = this.stockService.AddOrUpdate(data);
      stockViewModel.ID = stock.Id;
      LoadStockGrid();
      this.alertControl1.Show(this, "Save data Success !", "");
    }

    private void AddStockAction()
    {
      this.ViewModel = new StockViewModel()
      {
        CreatedDate = DateTime.Now
      };
      this.cboProduct.Focus();
    }

    private void DeleteStock(StockViewModel stockViewModel)
    {
      var data = mapper.Map<Stock>(stockViewModel);
      this.stockService.Delete(data);
      this.alertControl1.Show(this, "Data has removed !", "");

    }
 
    private void LoadStock(StockGridView stockGV)
    {
      this.ViewModel = mapper.Map<StockViewModel>(stockGV);

    }

    //private void txtSearchBaoCao_EditValueChanged(object sender, EventArgs e)
    //{
    //  SearchProdcutInStock();
    //}

    //private void SearchProdcutInStock()
    //{
    //  var productStocks = stockService.SearchStock(this.txtSearchBaoCao.Text);
    //  var dataGrids = mapper.Map<List<StockGridView>>(productStocks);
    //  mainBindingSource.DataSource = dataGrids;
    //}
  }
}