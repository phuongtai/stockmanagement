﻿namespace test1.Views
{
  partial class CustomerPage
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
      this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
      this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
      this.windowsUIButtonPanel1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
      this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
      this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
      this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
      ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
      this.panelControl2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
      this.panelControl3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
      this.splitContainerControl1.SuspendLayout();
      this.SuspendLayout();
      // 
      // labelControl1
      // 
      this.labelControl1.Location = new System.Drawing.Point(28, 38);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(63, 13);
      this.labelControl1.TabIndex = 1;
      this.labelControl1.Text = "labelControl1";
      // 
      // textEdit1
      // 
      this.textEdit1.Location = new System.Drawing.Point(139, 35);
      this.textEdit1.Name = "textEdit1";
      this.textEdit1.Size = new System.Drawing.Size(158, 20);
      this.textEdit1.TabIndex = 4;
      // 
      // panelControl2
      // 
      this.panelControl2.Controls.Add(this.windowsUIButtonPanel1);
      this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panelControl2.Location = new System.Drawing.Point(2, 425);
      this.panelControl2.Name = "panelControl2";
      this.panelControl2.Size = new System.Drawing.Size(515, 72);
      this.panelControl2.TabIndex = 6;
      // 
      // windowsUIButtonPanel1
      // 
      this.windowsUIButtonPanel1.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton(),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton()});
      this.windowsUIButtonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.windowsUIButtonPanel1.Location = new System.Drawing.Point(2, 2);
      this.windowsUIButtonPanel1.Name = "windowsUIButtonPanel1";
      this.windowsUIButtonPanel1.Size = new System.Drawing.Size(511, 68);
      this.windowsUIButtonPanel1.TabIndex = 1;
      this.windowsUIButtonPanel1.Text = "windowsUIButtonPanel1";
      // 
      // panelControl3
      // 
      this.panelControl3.Controls.Add(this.labelControl1);
      this.panelControl3.Controls.Add(this.textEdit1);
      this.panelControl3.Controls.Add(this.panelControl2);
      this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelControl3.Location = new System.Drawing.Point(0, 0);
      this.panelControl3.Name = "panelControl3";
      this.panelControl3.Size = new System.Drawing.Size(519, 499);
      this.panelControl3.TabIndex = 7;
      // 
      // dockManager1
      // 
      this.dockManager1.Form = this;
      this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl",
            "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl"});
      // 
      // splitContainerControl1
      // 
      this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
      this.splitContainerControl1.Name = "splitContainerControl1";
      this.splitContainerControl1.Panel1.Text = "Panel1";
      this.splitContainerControl1.Panel2.Controls.Add(this.panelControl3);
      this.splitContainerControl1.Panel2.Text = "Panel2";
      this.splitContainerControl1.Size = new System.Drawing.Size(702, 499);
      this.splitContainerControl1.SplitterPosition = 179;
      this.splitContainerControl1.TabIndex = 8;
      // 
      // CustomerPage
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.splitContainerControl1);
      this.Name = "CustomerPage";
      this.Size = new System.Drawing.Size(702, 499);
      this.Load += new System.EventHandler(this.CustomerPage_Load);
      ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
      this.panelControl2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
      this.panelControl3.ResumeLayout(false);
      this.panelControl3.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
      this.splitContainerControl1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.TextEdit textEdit1;
    private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
    private DevExpress.XtraEditors.PanelControl panelControl2;
    private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanel1;
    private DevExpress.XtraEditors.PanelControl panelControl3;
    private DevExpress.XtraBars.Docking.DockManager dockManager1;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
  }
}
