﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using test1.ViewModels;

namespace test1
{
  public partial class MainForm
  {
    private void LoadReportStockGrid()
    {
      this.gvReport.Columns.Clear();
      var data = reportService.GetDataReport();
      var dataGrids = mapper.Map<List<ReportStockGridView>>(data);
      grStock.DataSource = dataGrids;
    }

    private void LoadReportSaleOrderGrid()
    {
      this.gridView1.Columns.Clear();
      var saleOrders = reportService.GetAllOrders();

      var dataGrids = mapper.Map<List<OrderGridView>>(saleOrders);
      mainBindingSource.DataSource = dataGrids;
    }

    private void LoadReportSaleProductGrid()
    {
      this.gridView1.Columns.Clear();
      var saleProducts = reportService.GetAllSaleProducts();

      var dataGrids = mapper.Map<List<ProductOrderGridView>>(saleProducts);
      mainBindingSource.DataSource = dataGrids;
    }

    private void txtSearchBaoCao_EditValueChanged(object sender, EventArgs e)
    {
        SearchGridReport();
    }

    private void SearchGridReport()
    {
      var productOrders = reportService.SearchProduct(this.txtSearchBaoCao.Text);
      var dataGrids = mapper.Map<List<ReportStockGridView>>(productOrders);
      grStock.DataSource = dataGrids;
    }
  }
}