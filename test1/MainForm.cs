﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using test1.Services;
using test1.ViewModels;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using test1.Cores;
using AutoMapper;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid;
using test1.Reports;

namespace test1
{
  public partial class MainForm : BaseForm
  {
    private IMapper mapper;
    private string CurrentTab = "Customer";

    private CustomerService CustomerService;
    private ProductService productService;
    private OrderService orderService;
    private StockService stockService;
    private ReportService reportService;

    private IViewModel _viewModel;
    public IViewModel ViewModel
    {
      get { return _viewModel; }
      set {
        _viewModel = value;
        BindingControl();
      }
    }

    public MainForm(IMapper map, CustomerService customerService, ProductService productService, OrderService orderService, StockService stockService, ReportService reportService)
    {
      mapper = map;
      this.productService = productService;
      this.orderService = orderService;
      this.stockService = stockService;
      this.reportService = reportService;

      InitializeComponent();
      this.txtCustomer.Focus();

      this.CustomerService = customerService;
      ViewModel = new CustomerViewModel();
      //LoadCustomerGridContrl();
      LoadDefaultTab();
      this.ucOrderHistory1.PropertyChanged += OnOrderIdChanged;
    }

    private void LoadDefaultTab()
    {
      LoadControlOrder();
      AddOrderAction();
      this.NavMain.SelectedPage = this.orderPage;
      CurrentTab = "Order";
      gbInfo.Text = "Thông tin: Sản Phẩm";
    }

    private void OnOrderIdChanged(object sender, PropertyChangedEventArgs e)
    {
      var order = orderService.GetOderById(this.ucOrderHistory1.OrderId);
      this.tabOrder.SelectedPage = tbOrderPage;
      this.ViewModel = order;
    }

    private void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
    {

      switch (e.Button.Properties.Tag)
      {
        case "Customer":
          this.NavMain.SelectedPage = this.customerPage;
          LoadCustomerGridContrl();
          AddCustomerAction();
          CurrentTab = "Customer";
          gbInfo.Text = "Thông tin: Khách Hàng";
          break;
        case "Product":
          this.NavMain.SelectedPage = this.productPage;
          LoadDataControlProduct();
          
          CurrentTab = "Product";
          gbInfo.Text = "Thông tin: Sản Phẩm";
          break;
        case "Order":
          LoadControlOrder();
          AddOrderAction();
          this.NavMain.SelectedPage = this.orderPage;
          CurrentTab = "Order";
          gbInfo.Text = "Thông tin: Sản Phẩm";
          break;
        case "Stock":
          LoadControlStock();
          AddStockAction();
          this.NavMain.SelectedPage = this.stockPage;
          CurrentTab = "Stock";
          gbInfo.Text = "Thông tin: Sản Phẩm Nhập";
          break;
        case "Report":
          LoadReportStockGrid();
          LoadReportSaleOrderGrid();
          this.NavMain.SelectedPage = this.reportPage;
          gbInfo.Text = "Thông tin: Đơn Hàng";
          CurrentTab = "Report";
          break;
        default:
          break;
      }
    }

    private void btnPnAction_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
    {
      switch (e.Button.Properties.Tag.ToString())
      {
        case "Add":
          Add();
          break;

        case "Save":
          Save();
          break;

        case "Delete":
          if (this.ViewModel != null && this.ViewModel.ID > 0)
          {
            if(CurrentTab != "Order")
              Delete();
          }
          else MessageBox.Show("Hãy chọn 1 dòng trên lưới để xóa.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
          break;
      }

    }

    private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
    {
      GridView view = sender as GridView;
      var rows = view.GetSelectedRows();
      object row = view.GetRow(rows[0]);
      if (row != null)
      {
        LoadData(row);
      }
    }

    private void Add()
    {
      if (_viewModel is CustomerViewModel)
        AddCustomerAction();
      else if (_viewModel is ProductViewModel)
        AddProductAction();
      else if (_viewModel is OrderViewModel)
        AddOrderAction();
      else if (_viewModel is StockViewModel)
        AddStockAction();
    }

    private void Save()
    {
      if (CurrentTab == "Customer" && _viewModel is CustomerViewModel)
        AddOrUpdateCustomer((CustomerViewModel)this.ViewModel);
      else if (CurrentTab == "Product" && _viewModel is ProductViewModel)
        AddOrUpdateProduct((ProductViewModel)this.ViewModel);
      else if (CurrentTab == "Stock" && _viewModel is StockViewModel)
        AddOrUpdateStock((StockViewModel)this.ViewModel);
      else if (CurrentTab == "Order" && _viewModel is OrderViewModel)
      {
        if (string.IsNullOrEmpty(this.cboCustomer.Text))
        {
          MessageBox.Show("Hãy chọn khách hàng");
          return;
        }
        var data = this.cboCustomer.EditValue;
        var dataModel = (OrderViewModel)this.ViewModel;
        dataModel.CustomerId = (int)this.cboCustomer.EditValue;
        dataModel.Note = this.txtNote.Text;
        dataModel.CreatedDate = DateOrder.DateTime;
        this.ViewModel = SaveOrder(dataModel);
      }

    }

    private void Delete()
    {
      DialogResult result = MessageBox.Show("Bạn có muốn xóa data này ?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
      if (result == DialogResult.Yes)
      {
        if (_viewModel is CustomerViewModel)
        {
          DeleteCustomer((CustomerViewModel)this.ViewModel);
          this.ViewModel = new CustomerViewModel();
          LoadCustomerGridContrl();
        }
        else if (_viewModel is ProductViewModel)
        {
          DeleteProduct((ProductViewModel)this.ViewModel);
          this.ViewModel = new ProductViewModel();
          LoadProductGrid();
        }
        else if (_viewModel is StockViewModel)
        {
          DeleteStock((StockViewModel)this.ViewModel);
          this.ViewModel = new StockViewModel();
          LoadStockGrid();
        }

      }
    }

    private void BindingControl()
    {
      // customer
      if (_viewModel is CustomerViewModel)
        BindingCustomerControl((CustomerViewModel)_viewModel);
      else if (_viewModel is ProductViewModel)
        BindingProductControl((ProductViewModel)_viewModel);
      else if (_viewModel is StockViewModel)
        BindingStockControl((StockViewModel)_viewModel);
      else if (_viewModel is OrderViewModel)
        BindingOrderControl((OrderViewModel)_viewModel);

    }

    private void LoadData(object row)
    {
      if(CurrentTab == "Customer" && row.GetType() == typeof(CustomerGridView))
        LoadCustomer((CustomerGridView)row);
      else if(CurrentTab == "Product" && row.GetType() == typeof(ProductGridView))
        LoadProduct((ProductGridView)row);
      else if (CurrentTab == "Stock" && row.GetType() == typeof(StockGridView))
        LoadStock((StockGridView)row);
    }

    private void SearchGrid()
    {
      if (CurrentTab == "Customer")
      {
        var customers = CustomerService.SearchCustomer(this.txtSearch.Text);
        var dataGrids = mapper.Map<List<CustomerGridView>>(customers);
        mainBindingSource.DataSource = dataGrids;
      }
      if (CurrentTab == "Product" || CurrentTab == "Order")
      {
        var products = productService.SearchProduct(this.txtSearch.Text);

        var dataGrids = mapper.Map<List<ProductGridView>>(products);
        mainBindingSource.DataSource = dataGrids;
      }

    }

    private void gridView1_DoubleClick(object sender, EventArgs e)
    {
      if (CurrentTab == "Order")
      {
        DXMouseEventArgs ea = e as DXMouseEventArgs;
        GridView view = sender as GridView;
        GridHitInfo info = view.CalcHitInfo(ea.Location);
        if (info.RowInfo != null && (info.InRow || info.InRowCell))
        {
          var data = (ProductGridView)info.RowInfo.RowKey;
          var orderGridView = mapper.Map<ProductOrderViewModel>(data);
          AddProduct(orderGridView);
          
        }
      }
     
    }

    private void gridProduct_ProcessGridKey(object sender, KeyEventArgs e)
    {
      var grid = sender as GridControl;
      var view = grid.FocusedView as GridView;
      if (e.KeyData == Keys.Delete)
      {
        view.DeleteSelectedRows();
        e.Handled = true;
      }
    }

    private void gvProduct_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
    {
      GridView view = sender as GridView;
      if (view == null || e.Value == null) return;

      if (e.Column.FieldName == "Quantity" || e.Column.FieldName == "Price" || e.Column.FieldName == "gc15" || e.Column.FieldName == "gc17")
      {
        var total = CalTotal(e.Column.FieldName, view, e);
        view.SetRowCellValue(e.RowHandle, view.Columns["Total"], total);
      }
    }

    private decimal CalTotal(string fieldName, GridView view, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
    {
      var price = Convert.ToDecimal(view.GetListSourceRowCellValue(e.RowHandle, "Price"));
      var quality = Convert.ToDecimal(view.GetListSourceRowCellValue(e.RowHandle, "Quantity"));

      return price * quality;
    }

    private void CalPricePercent(string fieldName, GridView view, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
    {
      if (fieldName == "gc15")
      {
        var fixteenPercent = 15;

        var sCellPrice = $"{view.GetRowCellValue(e.RowHandle, view.Columns["Price"])}";
        decimal.TryParse(sCellPrice, out decimal price);
        var newPrice = Math.Round( (fixteenPercent * price) / 100 );
        view.SetRowCellValue(e.RowHandle, view.Columns["Price"], newPrice);
      }

      if (fieldName == "gc17")
      {
        var seventeenPercent = 17;

        var sCellPrice = $"{view.GetRowCellValue(e.RowHandle, view.Columns["Price"])}";
        decimal.TryParse(sCellPrice, out decimal price);
        var newPrice = Math.Round((seventeenPercent * price) / 100);
        view.SetRowCellValue(e.RowHandle, view.Columns["Price"], newPrice);
      }

    }

    private void txtSearch_EditValueChanged(object sender, EventArgs e)
    {
      SearchGrid();
    }

    private void MainForm_Load(object sender, EventArgs e)
    {

    }

    private void btnReport_Click(object sender, EventArgs e)
    {
      ReportParameter reportParameter = new ReportParameter()
      {
        Id = ViewModel.ID
      };
      ReportViewer reportViewer = new ReportViewer(reportParameter);
      reportViewer.WindowState = FormWindowState.Maximized;
      reportViewer.ShowProductOrderReport();
    }

    private void tabPane1_SelectedPageChanged(object sender, DevExpress.XtraBars.Navigation.SelectedPageChangedEventArgs e)
    {
      if (e.Page.Caption == "History")
      {
        this.ucOrderHistory1.LoadData(this.orderService);
      }
    }

    private void gvProduct_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
    {
      GridView view = sender as GridView;
      if (e.Column.FieldName == "gc15")
      {
        var data = (ProductOrderViewModel)e.Row;
        int? percent15 = 15;
        if (e.IsSetData)
        {
          if (e.Value.Equals(true))
          {
            data.PricePercent = percent15;
            var discount = (data.OriginalPrice * percent15.Value) / 100;
            data.Price = data.OriginalPrice - discount;
          }
          else
          {
            data.PricePercent = null;
            data.Price = data.OriginalPrice;
          }
        }
        else if (e.IsGetData)
        {
          e.Value = data.PricePercent.HasValue && data.PricePercent.Value == percent15;
        }

      }

      if (e.Column.FieldName == "gc17")
      {
        var data = (ProductOrderViewModel)e.Row;
        int? percent17 = 17;
        if (e.IsSetData)
        {
          if (e.Value.Equals(true))
          {
            data.PricePercent = percent17;
            var discount = (data.OriginalPrice * percent17.Value) / 100;
            data.Price = data.OriginalPrice - discount;
          }
          else
          {
            data.PricePercent = null;
            data.Price = data.OriginalPrice;
            
          }

        }
        else if(e.IsGetData)
        {
          e.Value = data.PricePercent.HasValue && data.PricePercent.Value == percent17;
        }
      }
    }
    
    private void gvProduct_MouseLeave(object sender, EventArgs e)
    {
      this.gvProduct.PostEditor();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      ReportViewer reportViewer = new ReportViewer(new ReportParameter());
      reportViewer.WindowState = FormWindowState.Maximized;
      reportViewer.ShowProductAmounReport();
    }

  }
}