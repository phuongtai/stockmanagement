﻿namespace test1
{
  partial class ucOrderHistory
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
      DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
      DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
      DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
      DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
      DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucOrderHistory));
      DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
      DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
      DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
      DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
      DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
      this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
      this.repoItBtnEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
      this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
      this.pnDocumentManager = new DevExpress.XtraEditors.PanelControl();
      this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
      this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
      this.GrdOrderHistory = new DevExpress.XtraGrid.GridControl();
      this.mainBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.gvProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
      gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
      gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
      gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
      gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
      gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
      ((System.ComponentModel.ISupportInitialize)(this.repoItBtnEdit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
      this.groupControl1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pnDocumentManager)).BeginInit();
      this.pnDocumentManager.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
      this.groupControl2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.GrdOrderHistory)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.mainBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvProduct)).BeginInit();
      this.SuspendLayout();
      // 
      // gridColumn1
      // 
      gridColumn1.Caption = "Tên Khách Hàng";
      gridColumn1.FieldName = "CustomerName";
      gridColumn1.MinWidth = 27;
      gridColumn1.Name = "gridColumn1";
      gridColumn1.OptionsColumn.AllowEdit = false;
      gridColumn1.OptionsColumn.ReadOnly = true;
      gridColumn1.Visible = true;
      gridColumn1.VisibleIndex = 1;
      gridColumn1.Width = 100;
      // 
      // gridColumn3
      // 
      gridColumn3.Caption = "Ngày Bán";
      gridColumn3.FieldName = "CreatedDate";
      gridColumn3.MinWidth = 27;
      gridColumn3.Name = "gridColumn3";
      gridColumn3.Visible = true;
      gridColumn3.VisibleIndex = 2;
      gridColumn3.Width = 100;
      // 
      // gridColumn2
      // 
      gridColumn2.Caption = "Tổng Tiền";
      gridColumn2.FieldName = "Total";
      gridColumn2.MinWidth = 27;
      gridColumn2.Name = "gridColumn2";
      gridColumn2.Visible = true;
      gridColumn2.VisibleIndex = 3;
      gridColumn2.Width = 100;
      // 
      // gridColumn4
      // 
      gridColumn4.Caption = "Ghi Chú";
      gridColumn4.FieldName = "Note";
      gridColumn4.MinWidth = 27;
      gridColumn4.Name = "gridColumn4";
      gridColumn4.OptionsColumn.AllowEdit = false;
      gridColumn4.OptionsColumn.AllowMove = false;
      gridColumn4.OptionsColumn.ReadOnly = true;
      gridColumn4.Visible = true;
      gridColumn4.VisibleIndex = 4;
      gridColumn4.Width = 100;
      // 
      // gridColumn6
      // 
      gridColumn6.Caption = "STT";
      gridColumn6.FieldName = "STT";
      gridColumn6.MinWidth = 27;
      gridColumn6.Name = "gridColumn6";
      gridColumn6.Visible = true;
      gridColumn6.VisibleIndex = 0;
      gridColumn6.Width = 100;
      // 
      // gridColumn5
      // 
      this.gridColumn5.Caption = "Click";
      this.gridColumn5.ColumnEdit = this.repoItBtnEdit;
      this.gridColumn5.FieldName = "OrderId";
      this.gridColumn5.ImageOptions.Alignment = System.Drawing.StringAlignment.Center;
      this.gridColumn5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("gridColumn5.ImageOptions.Image")));
      this.gridColumn5.MinWidth = 27;
      this.gridColumn5.Name = "gridColumn5";
      this.gridColumn5.Width = 100;
      // 
      // repoItBtnEdit
      // 
      this.repoItBtnEdit.AutoHeight = false;
      editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
      this.repoItBtnEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "Cập Nhật", 20, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
      this.repoItBtnEdit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
      this.repoItBtnEdit.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
      this.repoItBtnEdit.Name = "repoItBtnEdit";
      this.repoItBtnEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
      // 
      // groupControl1
      // 
      this.groupControl1.Controls.Add(this.pnDocumentManager);
      this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
      this.groupControl1.Location = new System.Drawing.Point(0, 0);
      this.groupControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.groupControl1.Name = "groupControl1";
      this.groupControl1.Size = new System.Drawing.Size(757, 86);
      this.groupControl1.TabIndex = 4;
      this.groupControl1.Text = "Chọn thông tin khách hàng";
      // 
      // pnDocumentManager
      // 
      this.pnDocumentManager.Controls.Add(this.btnEdit);
      this.pnDocumentManager.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnDocumentManager.Location = new System.Drawing.Point(2, 27);
      this.pnDocumentManager.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.pnDocumentManager.Name = "pnDocumentManager";
      this.pnDocumentManager.Size = new System.Drawing.Size(753, 57);
      this.pnDocumentManager.TabIndex = 0;
      // 
      // btnEdit
      // 
      this.btnEdit.Location = new System.Drawing.Point(269, 17);
      this.btnEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.Size = new System.Drawing.Size(173, 28);
      this.btnEdit.TabIndex = 6;
      this.btnEdit.Text = "Cập Nhật Đơn Hàng";
      this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
      // 
      // groupControl2
      // 
      this.groupControl2.Controls.Add(this.GrdOrderHistory);
      this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupControl2.Location = new System.Drawing.Point(0, 86);
      this.groupControl2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.groupControl2.Name = "groupControl2";
      this.groupControl2.Size = new System.Drawing.Size(757, 442);
      this.groupControl2.TabIndex = 5;
      this.groupControl2.Text = "Thông tin sản phẩm ";
      // 
      // GrdOrderHistory
      // 
      this.GrdOrderHistory.DataSource = this.mainBindingSource;
      this.GrdOrderHistory.Dock = System.Windows.Forms.DockStyle.Fill;
      this.GrdOrderHistory.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      gridLevelNode1.RelationName = "Level1";
      this.GrdOrderHistory.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
      this.GrdOrderHistory.Location = new System.Drawing.Point(2, 27);
      this.GrdOrderHistory.MainView = this.gvProduct;
      this.GrdOrderHistory.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.GrdOrderHistory.Name = "GrdOrderHistory";
      this.GrdOrderHistory.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repoItBtnEdit});
      this.GrdOrderHistory.Size = new System.Drawing.Size(753, 413);
      this.GrdOrderHistory.TabIndex = 1;
      this.GrdOrderHistory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvProduct});
      this.GrdOrderHistory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.GrdOrderHistory_MouseDoubleClick);
      // 
      // mainBindingSource
      // 
      this.mainBindingSource.DataSource = typeof(test1.Customer);
      // 
      // gvProduct
      // 
      this.gvProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            gridColumn6,
            gridColumn1,
            gridColumn3,
            gridColumn2,
            gridColumn4,
            this.gridColumn5});
      this.gvProduct.DetailHeight = 431;
      this.gvProduct.GridControl = this.GrdOrderHistory;
      this.gvProduct.Name = "gvProduct";
      this.gvProduct.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
      this.gvProduct.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
      this.gvProduct.OptionsBehavior.Editable = false;
      this.gvProduct.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
      // 
      // ucOrderHistory
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.groupControl2);
      this.Controls.Add(this.groupControl1);
      this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.Name = "ucOrderHistory";
      this.Size = new System.Drawing.Size(757, 528);
      ((System.ComponentModel.ISupportInitialize)(this.repoItBtnEdit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
      this.groupControl1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pnDocumentManager)).EndInit();
      this.pnDocumentManager.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
      this.groupControl2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.GrdOrderHistory)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.mainBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gvProduct)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl groupControl1;
    private DevExpress.XtraEditors.PanelControl pnDocumentManager;
    private DevExpress.XtraEditors.SimpleButton btnEdit;
    private DevExpress.XtraEditors.GroupControl groupControl2;
    private DevExpress.XtraGrid.GridControl GrdOrderHistory;
    private DevExpress.XtraGrid.Views.Grid.GridView gvProduct;
    private System.Windows.Forms.BindingSource mainBindingSource;
    private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repoItBtnEdit;
  }
}
