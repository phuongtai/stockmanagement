﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using test1.Services;
using test1.ViewModels;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using test1.Cores;
using AutoMapper;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

namespace test1
{
  public partial class MainForm
  {

    private List<ProductOrderViewModel> productOrderSelects = new List<ProductOrderViewModel>();

    private void LoadControlOrder()
    {
      LoadComboxCustomer();
      LoadProductGrid();
    }

    private void LoadComboxCustomer()
    {
      var customers = this.CustomerService.GetCustomers();
      var customerModels = mapper.Map<List<CustomerViewModel>>(customers);
      this.cboCustomer.Properties.DataSource = customerModels;
    }

    private void AddProduct(ProductOrderViewModel product)
    {
      var hasInsert = productOrderSelects.Any(p => p.ProductId == product.ProductId);
      if (!hasInsert)
      {
        productOrderSelects.Add(product);
        this.gridProduct.DataSource = null;
        this.gridProduct.DataSource = productOrderSelects;
      }

    } 
 
    private OrderViewModel SaveOrder(OrderViewModel orderData)
    {
      if (orderData.CustomerId == 0)
      {
        MessageBox.Show("Hãy chọn khách hàng");
        return orderData;
      }
      if (productOrderSelects.Count == 0)
      {
        MessageBox.Show("Hãy chọn sản phẩm");
        return orderData;
      }

      // Save Order
      var order = this.orderService.Save(orderData, productOrderSelects);

      this.alertControl1.Show(this, "Save data Success !", "");
      return order;
    }

    private void AddOrderAction()
    {
      this.ViewModel = new OrderViewModel() { CreatedDate = DateTime.Now };
      for (int i = 0; i < gvProduct.RowCount;)
        gvProduct.DeleteRow(i);
      this.cboCustomer.EditValue = 0;

    }

    private void DeleteOrder(OrderViewModel productViewModel)
    {
      if (productService.CanDelete(productViewModel.ID))
      {
        var data = mapper.Map<Product>(productViewModel);
        this.productService.Delete(data);
        this.alertControl1.Show(this, "Data has removed !", "");
      }
      else
      {
        MessageBox.Show("- Khách hàng này ko thể xóa. Vì khách hàng có tạo đơn hàng. !", "Cảnh Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      
    }
 
    private void LoadOrder(OrderViewModel productGridView)
    {
      this.ViewModel = mapper.Map<ProductViewModel>(productGridView);
    }

    private void BindingOrderControl(OrderViewModel orderViewModel)
    {
      this.txtNote.DataBindings.Clear();
      this.txtNote.DataBindings.Add("Text", orderViewModel, "Note", false, DataSourceUpdateMode.OnPropertyChanged);

      this.cboCustomer.DataBindings.Clear();
      this.cboCustomer.DataBindings.Add("EditValue", orderViewModel, "CustomerId", false, DataSourceUpdateMode.OnPropertyChanged);

      this.DateOrder.DataBindings.Clear();
      this.DateOrder.DataBindings.Add("EditValue", orderViewModel, "CreatedDate", false, DataSourceUpdateMode.OnPropertyChanged);

      if (orderViewModel.Products != null)
      {
        LoadProductOrderGrid(orderViewModel.Products);
      }
    }

    private void LoadProductOrderGrid(List<ProductOrderViewModel> products)
    {
      this.gridProduct.DataSource = null;
      productOrderSelects.Clear();
      productOrderSelects.AddRange(products);
      this.gridProduct.DataSource = productOrderSelects;
    }

  }
}