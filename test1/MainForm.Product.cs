﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using test1.Services;
using test1.ViewModels;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using test1.Cores;
using AutoMapper;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;

namespace test1
{
  public partial class MainForm
  {

    private void LoadDataControlProduct()
    {
      LoadProductGrid();
      AddProductAction();
      LoadUnits();
    }

    private void LoadUnits()
    {
      this.cboUnit.Properties.Items.Clear();
      var units = productService.GetUnits();
      var i = 0;
      foreach (var item in units)
      {
        this.cboUnit.Properties.Items.Add(new ImageComboBoxItem()
        {
          Description = item.Label,
          ImageIndex = i <= 4 ? i : 4,
          Value = item.Id
        });
        i++;
      }
    }

    private List<ProductGridView> ProductGridViews = new List<ProductGridView>();
    private void BindingProductControl(ProductViewModel productVM)
    {
      this.txtPaintName.DataBindings.Clear();
      this.txtPaintName.DataBindings.Add("Text", productVM, "Name", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtPaintCode.DataBindings.Clear();
      this.txtPaintCode.DataBindings.Add("Text", productVM, "Code", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtPrice.DataBindings.Clear();
      this.txtPrice.DataBindings.Add("Text", productVM, "Price", false, DataSourceUpdateMode.OnPropertyChanged);

      this.cboUnit.DataBindings.Clear();
      this.cboUnit.DataBindings.Add("EditValue", productVM, "UnitId", false, DataSourceUpdateMode.OnPropertyChanged);

    }

    private void LoadProductGrid()
    {
      this.gridView1.Columns.Clear();
      var products = productService.GetProducts();

      var dataGrids = mapper.Map<List<ProductGridView>>(products);
      mainBindingSource.DataSource = dataGrids;
      ProductGridViews = dataGrids;
    }
 
    private void AddOrUpdateProduct(ProductViewModel productViewModel)
    {
      if (!Validation(productViewModel)) return;

      if (productViewModel.UnitId == 0)
      {
        MessageBox.Show("Hãy chọn đơn vị sơn", "Warning", MessageBoxButtons.OK, icon: MessageBoxIcon.Error);
        return;
      }

      var data = mapper.Map<Product>(productViewModel);
      data.LastChanged = DateTime.Now;
      var product = this.productService.AddOrUpdate(data);
      productViewModel.ID = product.Id;
      LoadProductGrid();
      this.alertControl1.Show(this, "Save data Success !", "");
    }

    private void AddProductAction()
    {
      this.ViewModel = new ProductViewModel();
      this.txtPaintName.Focus();
    }

    private void DeleteProduct(ProductViewModel productViewModel)
    {
      if (productService.CanDelete(productViewModel.ID))
      {
        var data = mapper.Map<Product>(productViewModel);
        this.productService.Delete(data);
        this.alertControl1.Show(this, "Data has removed !", "");
      }
      else
      {
        MessageBox.Show("- Khách hàng này ko thể xóa. Vì khách hàng có tạo đơn hàng. !", "Cảnh Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      
    }
 
    private void LoadProduct(ProductGridView productGridView)
    {
      this.ViewModel = mapper.Map<ProductViewModel>(productGridView);

    }
     
  }
}