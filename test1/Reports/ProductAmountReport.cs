﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace test1.Reports
{
  public partial class ProductAmountReport : DevExpress.XtraReports.UI.XtraReport
  {
    ReportParameter reportParameter;

    public ProductAmountReport()
    {
      InitializeComponent();
    }
    public ProductAmountReport(ReportParameter reportParameter)
    {
      this.reportParameter = reportParameter;
      InitializeComponent();
      Init(reportParameter.Id);
    }

    public void Init(int orderId)
    {

        this.efDataSourceProductAmount.StoredProcedures.Clear();
        DevExpress.DataAccess.EntityFramework.EFConnectionParameters efConnectionParameters1 = new DevExpress.DataAccess.EntityFramework.EFConnectionParameters();
        DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo efStoredProcedureInfo1 = new DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo();
        DevExpress.DataAccess.EntityFramework.EFParameter efParameter1 = new DevExpress.DataAccess.EntityFramework.EFParameter();

        efConnectionParameters1.ConnectionStringName = "PaintManagementContext";
        efConnectionParameters1.Source = typeof(test1.PaintManagementContext);
        this.efDataSourceProductAmount.ConnectionParameters = efConnectionParameters1;
        this.efDataSourceProductAmount.Name = "efDataSourceProductAmount";
        efStoredProcedureInfo1.Name = "SP_GET_TotalProductAmount";
        
        this.efDataSourceProductAmount.StoredProcedures.AddRange(new DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo[] {
        efStoredProcedureInfo1});


    }

  }
}
