﻿namespace test1.Reports
{
  partial class ProductAmountReport
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      DevExpress.DataAccess.EntityFramework.EFConnectionParameters efConnectionParameters1 = new DevExpress.DataAccess.EntityFramework.EFConnectionParameters();
      DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo efStoredProcedureInfo1 = new DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo();
      this.Detail = new DevExpress.XtraReports.UI.DetailBand();
      this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
      this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
      this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.invoiceInfoTable = new DevExpress.XtraReports.UI.XRTable();
      this.invoiceInfoTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
      this.invoiceLabel = new DevExpress.XtraReports.UI.XRTableCell();
      this.customerTable = new DevExpress.XtraReports.UI.XRTable();
      this.customerCountryRow = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
      this.customerCountry = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
      this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
      this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
      this.totalTable = new DevExpress.XtraReports.UI.XRTable();
      this.totalRow = new DevExpress.XtraReports.UI.XRTableRow();
      this.totalCaption = new DevExpress.XtraReports.UI.XRTableCell();
      this.total = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.headerTable = new DevExpress.XtraReports.UI.XRTable();
      this.headerTableRow = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
      this.quantityCaption = new DevExpress.XtraReports.UI.XRTableCell();
      this.productNameCaption = new DevExpress.XtraReports.UI.XRTableCell();
      this.unitPriceCaption = new DevExpress.XtraReports.UI.XRTableCell();
      this.lineTotalCaption = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
      this.thankYouLabel = new DevExpress.XtraReports.UI.XRLabel();
      this.efDataSourceProductAmount = new DevExpress.DataAccess.EntityFramework.EFDataSource(this.components);
      this.lineTotal = new DevExpress.XtraReports.UI.XRTableCell();
      this.unitPrice = new DevExpress.XtraReports.UI.XRTableCell();
      this.productName = new DevExpress.XtraReports.UI.XRTableCell();
      this.quantity = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
      this.detailTableRow = new DevExpress.XtraReports.UI.XRTableRow();
      this.detailTable = new DevExpress.XtraReports.UI.XRTable();
      ((System.ComponentModel.ISupportInitialize)(this.invoiceInfoTable)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.customerTable)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.totalTable)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.headerTable)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.efDataSourceProductAmount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.detailTable)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      // 
      // Detail
      // 
      this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.detailTable});
      this.Detail.Dpi = 254F;
      this.Detail.HeightF = 58F;
      this.Detail.KeepTogether = true;
      this.Detail.Name = "Detail";
      this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
      // 
      // TopMargin
      // 
      this.TopMargin.Dpi = 254F;
      this.TopMargin.HeightF = 76F;
      this.TopMargin.Name = "TopMargin";
      this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.TopMargin.StylePriority.UseBackColor = false;
      this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
      // 
      // BottomMargin
      // 
      this.BottomMargin.Dpi = 254F;
      this.BottomMargin.HeightF = 201F;
      this.BottomMargin.Name = "BottomMargin";
      this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
      this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
      // 
      // GroupHeader2
      // 
      this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.invoiceInfoTable,
            this.customerTable});
      this.GroupHeader2.Dpi = 254F;
      this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("InvoiceNumber", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
      this.GroupHeader2.HeightF = 299F;
      this.GroupHeader2.Level = 1;
      this.GroupHeader2.Name = "GroupHeader2";
      // 
      // invoiceInfoTable
      // 
      this.invoiceInfoTable.Dpi = 254F;
      this.invoiceInfoTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.invoiceInfoTable.Name = "invoiceInfoTable";
      this.invoiceInfoTable.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 25, 0, 0, 254F);
      this.invoiceInfoTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.invoiceInfoTableRow1});
      this.invoiceInfoTable.SizeF = new System.Drawing.SizeF(1648.46F, 215.9001F);
      this.invoiceInfoTable.StylePriority.UsePadding = false;
      // 
      // invoiceInfoTableRow1
      // 
      this.invoiceInfoTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.invoiceLabel});
      this.invoiceInfoTableRow1.Dpi = 254F;
      this.invoiceInfoTableRow1.Name = "invoiceInfoTableRow1";
      this.invoiceInfoTableRow1.Weight = 1.4541063764010338D;
      // 
      // xrTableCell6
      // 
      this.xrTableCell6.Dpi = 254F;
      this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.xrTableCell6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.xrTableCell6.Multiline = true;
      this.xrTableCell6.Name = "xrTableCell6";
      this.xrTableCell6.StylePriority.UseFont = false;
      this.xrTableCell6.StylePriority.UseForeColor = false;
      this.xrTableCell6.StylePriority.UseTextAlignment = false;
      this.xrTableCell6.Text = "DNTN NGỌC CHÂU BẾN LỨC\r\nĐC : Ấp 8, Xã Lương Hòa, Bến Lức, Long An";
      this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.xrTableCell6.Weight = 2.1300785056442111D;
      // 
      // invoiceLabel
      // 
      this.invoiceLabel.Dpi = 254F;
      this.invoiceLabel.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.invoiceLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.invoiceLabel.Multiline = true;
      this.invoiceLabel.Name = "invoiceLabel";
      this.invoiceLabel.StylePriority.UseFont = false;
      this.invoiceLabel.StylePriority.UseForeColor = false;
      this.invoiceLabel.StylePriority.UseTextAlignment = false;
      this.invoiceLabel.Text = "Sản Phẩm Tồn Kho";
      this.invoiceLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.invoiceLabel.Weight = 1.7423837598785747D;
      // 
      // customerTable
      // 
      this.customerTable.Dpi = 254F;
      this.customerTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 215.9001F);
      this.customerTable.Name = "customerTable";
      this.customerTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.customerCountryRow});
      this.customerTable.SizeF = new System.Drawing.SizeF(1648F, 76.34595F);
      // 
      // customerCountryRow
      // 
      this.customerCountryRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.customerCountry});
      this.customerCountryRow.Dpi = 254F;
      this.customerCountryRow.Name = "customerCountryRow";
      this.customerCountryRow.Weight = 0.93333357069233458D;
      // 
      // xrTableCell7
      // 
      this.xrTableCell7.Dpi = 254F;
      this.xrTableCell7.Multiline = true;
      this.xrTableCell7.Name = "xrTableCell7";
      this.xrTableCell7.StylePriority.UseTextAlignment = false;
      this.xrTableCell7.Text = "Ngày Báo Cáo :        ";
      this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell7.Weight = 6.2213309162673722D;
      // 
      // customerCountry
      // 
      this.customerCountry.Dpi = 254F;
      this.customerCountry.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CreatedDate]")});
      this.customerCountry.Name = "customerCountry";
      this.customerCountry.StylePriority.UseTextAlignment = false;
      this.customerCountry.Text = "CustomerCountry";
      this.customerCountry.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.customerCountry.Weight = 6.852693100177528D;
      // 
      // GroupFooter1
      // 
      this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.totalTable});
      this.GroupFooter1.Dpi = 254F;
      this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
      this.GroupFooter1.HeightF = 179.6167F;
      this.GroupFooter1.KeepTogether = true;
      this.GroupFooter1.Name = "GroupFooter1";
      // 
      // xrLabel3
      // 
      this.xrLabel3.Dpi = 254F;
      this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(657.2079F, 101.5461F);
      this.xrLabel3.Multiline = true;
      this.xrLabel3.Name = "xrLabel3";
      this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel3.SizeF = new System.Drawing.SizeF(291.0415F, 58.42F);
      this.xrLabel3.StylePriority.UseTextAlignment = false;
      this.xrLabel3.Text = "NGƯỜI GIAO";
      this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      // 
      // xrLabel2
      // 
      this.xrLabel2.Dpi = 254F;
      this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(59.79584F, 101.5461F);
      this.xrLabel2.Multiline = true;
      this.xrLabel2.Name = "xrLabel2";
      this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel2.SizeF = new System.Drawing.SizeF(351.8959F, 58.42F);
      this.xrLabel2.StylePriority.UseTextAlignment = false;
      this.xrLabel2.Text = "NGƯỜI NHẬN HÀNG";
      this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      // 
      // xrLabel1
      // 
      this.xrLabel1.Dpi = 254F;
      this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(1226.185F, 101.5461F);
      this.xrLabel1.Multiline = true;
      this.xrLabel1.Name = "xrLabel1";
      this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.xrLabel1.SizeF = new System.Drawing.SizeF(396.8752F, 58.42F);
      this.xrLabel1.StylePriority.UseTextAlignment = false;
      this.xrLabel1.Text = "NGƯỜI VIẾT HOA ĐƠN";
      this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      // 
      // totalTable
      // 
      this.totalTable.Dpi = 254F;
      this.totalTable.ForeColor = System.Drawing.Color.Black;
      this.totalTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.totalTable.Name = "totalTable";
      this.totalTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.totalRow});
      this.totalTable.SizeF = new System.Drawing.SizeF(1648F, 66.22463F);
      this.totalTable.StylePriority.UseForeColor = false;
      // 
      // totalRow
      // 
      this.totalRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.totalCaption,
            this.total});
      this.totalRow.Dpi = 254F;
      this.totalRow.Name = "totalRow";
      this.totalRow.Weight = 1.4D;
      // 
      // totalCaption
      // 
      this.totalCaption.BackColor = System.Drawing.Color.Gainsboro;
      this.totalCaption.Dpi = 254F;
      this.totalCaption.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.totalCaption.Name = "totalCaption";
      this.totalCaption.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 25, 0, 0, 254F);
      this.totalCaption.StylePriority.UseBackColor = false;
      this.totalCaption.StylePriority.UseFont = false;
      this.totalCaption.StylePriority.UsePadding = false;
      this.totalCaption.StylePriority.UseTextAlignment = false;
      this.totalCaption.Text = "Tổng Cộng";
      this.totalCaption.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.totalCaption.Weight = 2.873326575465724D;
      // 
      // total
      // 
      this.total.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.total.Dpi = 254F;
      this.total.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Sum([TotalPriceInStock])")});
      this.total.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.total.Name = "total";
      this.total.StylePriority.UseBorders = false;
      this.total.StylePriority.UseFont = false;
      this.total.StylePriority.UseTextAlignment = false;
      this.total.Text = "₫0.00";
      this.total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.total.TextFormatString = "{0:#,#}";
      this.total.Weight = 1.0662250349613316D;
      // 
      // GroupHeader1
      // 
      this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.headerTable});
      this.GroupHeader1.Dpi = 254F;
      this.GroupHeader1.HeightF = 90F;
      this.GroupHeader1.Name = "GroupHeader1";
      this.GroupHeader1.RepeatEveryPage = true;
      // 
      // headerTable
      // 
      this.headerTable.Dpi = 254F;
      this.headerTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.headerTable.Name = "headerTable";
      this.headerTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.headerTableRow});
      this.headerTable.SizeF = new System.Drawing.SizeF(1648.46F, 88.89993F);
      this.headerTable.StylePriority.UsePadding = false;
      // 
      // headerTableRow
      // 
      this.headerTableRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell1,
            this.quantityCaption,
            this.productNameCaption,
            this.unitPriceCaption,
            this.lineTotalCaption});
      this.headerTableRow.Dpi = 254F;
      this.headerTableRow.Name = "headerTableRow";
      this.headerTableRow.Weight = 11.5D;
      // 
      // xrTableCell2
      // 
      this.xrTableCell2.BackColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell2.Dpi = 254F;
      this.xrTableCell2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell2.Multiline = true;
      this.xrTableCell2.Name = "xrTableCell2";
      this.xrTableCell2.StylePriority.UseBackColor = false;
      this.xrTableCell2.StylePriority.UseBorders = false;
      this.xrTableCell2.StylePriority.UseFont = false;
      this.xrTableCell2.StylePriority.UseTextAlignment = false;
      this.xrTableCell2.Text = "STT";
      this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.xrTableCell2.Weight = 0.14321881732823844D;
      // 
      // xrTableCell1
      // 
      this.xrTableCell1.BackColor = System.Drawing.Color.Gainsboro;
      this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell1.Dpi = 254F;
      this.xrTableCell1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
      this.xrTableCell1.Multiline = true;
      this.xrTableCell1.Name = "xrTableCell1";
      this.xrTableCell1.StylePriority.UseBackColor = false;
      this.xrTableCell1.StylePriority.UseBorders = false;
      this.xrTableCell1.StylePriority.UseFont = false;
      this.xrTableCell1.StylePriority.UseTextAlignment = false;
      this.xrTableCell1.Text = "TÊN SẢN PHẨM";
      this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.xrTableCell1.Weight = 1.0412490257671805D;
      // 
      // quantityCaption
      // 
      this.quantityCaption.BackColor = System.Drawing.Color.Gainsboro;
      this.quantityCaption.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.quantityCaption.Dpi = 254F;
      this.quantityCaption.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
      this.quantityCaption.Name = "quantityCaption";
      this.quantityCaption.StylePriority.UseBackColor = false;
      this.quantityCaption.StylePriority.UseBorders = false;
      this.quantityCaption.StylePriority.UseFont = false;
      this.quantityCaption.StylePriority.UseTextAlignment = false;
      this.quantityCaption.Text = "Mã Sơn";
      this.quantityCaption.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.quantityCaption.Weight = 0.22050334345262521D;
      // 
      // productNameCaption
      // 
      this.productNameCaption.BackColor = System.Drawing.Color.Gainsboro;
      this.productNameCaption.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.productNameCaption.Dpi = 254F;
      this.productNameCaption.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
      this.productNameCaption.Name = "productNameCaption";
      this.productNameCaption.StylePriority.UseBackColor = false;
      this.productNameCaption.StylePriority.UseBorders = false;
      this.productNameCaption.StylePriority.UseFont = false;
      this.productNameCaption.StylePriority.UsePadding = false;
      this.productNameCaption.StylePriority.UseTextAlignment = false;
      this.productNameCaption.Text = "SL";
      this.productNameCaption.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.productNameCaption.Weight = 0.13304802578587791D;
      // 
      // unitPriceCaption
      // 
      this.unitPriceCaption.BackColor = System.Drawing.Color.Gainsboro;
      this.unitPriceCaption.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.unitPriceCaption.Dpi = 254F;
      this.unitPriceCaption.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
      this.unitPriceCaption.Name = "unitPriceCaption";
      this.unitPriceCaption.StylePriority.UseBackColor = false;
      this.unitPriceCaption.StylePriority.UseBorders = false;
      this.unitPriceCaption.StylePriority.UseFont = false;
      this.unitPriceCaption.StylePriority.UseTextAlignment = false;
      this.unitPriceCaption.Text = "Đơn Giá";
      this.unitPriceCaption.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.unitPriceCaption.Weight = 0.27659014334984261D;
      // 
      // lineTotalCaption
      // 
      this.lineTotalCaption.BackColor = System.Drawing.Color.Gainsboro;
      this.lineTotalCaption.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lineTotalCaption.Dpi = 254F;
      this.lineTotalCaption.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
      this.lineTotalCaption.Name = "lineTotalCaption";
      this.lineTotalCaption.StylePriority.UseBackColor = false;
      this.lineTotalCaption.StylePriority.UseBorders = false;
      this.lineTotalCaption.StylePriority.UseFont = false;
      this.lineTotalCaption.StylePriority.UseTextAlignment = false;
      this.lineTotalCaption.Text = "Thành Tiền";
      this.lineTotalCaption.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.lineTotalCaption.Weight = 0.36338372063949648D;
      // 
      // GroupFooter2
      // 
      this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.thankYouLabel});
      this.GroupFooter2.Dpi = 254F;
      this.GroupFooter2.HeightF = 428F;
      this.GroupFooter2.KeepTogether = true;
      this.GroupFooter2.Level = 1;
      this.GroupFooter2.Name = "GroupFooter2";
      this.GroupFooter2.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBandExceptLastEntry;
      this.GroupFooter2.PrintAtBottom = true;
      // 
      // thankYouLabel
      // 
      this.thankYouLabel.CanShrink = true;
      this.thankYouLabel.Dpi = 254F;
      this.thankYouLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
      this.thankYouLabel.LocationFloat = new DevExpress.Utils.PointFloat(0F, 51.3292F);
      this.thankYouLabel.Name = "thankYouLabel";
      this.thankYouLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
      this.thankYouLabel.SizeF = new System.Drawing.SizeF(1648F, 63.49994F);
      this.thankYouLabel.StylePriority.UseFont = false;
      this.thankYouLabel.StylePriority.UseTextAlignment = false;
      this.thankYouLabel.Text = "Trân Trọng Cám Ơn !";
      this.thankYouLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      // 
      // efDataSourceProductAmount
      // 
      efConnectionParameters1.ConnectionString = "";
      efConnectionParameters1.ConnectionStringName = "PaintManagementContext";
      efConnectionParameters1.Source = typeof(test1.PaintManagementContext);
      this.efDataSourceProductAmount.ConnectionParameters = efConnectionParameters1;
      this.efDataSourceProductAmount.Name = "efDataSourceProductAmount";
      efStoredProcedureInfo1.Name = "SP_GET_TotalProductAmount";
      this.efDataSourceProductAmount.StoredProcedures.AddRange(new DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo[] {
            efStoredProcedureInfo1});
      // 
      // lineTotal
      // 
      this.lineTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.lineTotal.Dpi = 254F;
      this.lineTotal.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SP_GET_TotalProductAmount].[TotalPriceInStock]")});
      this.lineTotal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
      this.lineTotal.Name = "lineTotal";
      this.lineTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 13, 0, 254F);
      this.lineTotal.StylePriority.UseBorders = false;
      this.lineTotal.StylePriority.UseFont = false;
      this.lineTotal.StylePriority.UseForeColor = false;
      this.lineTotal.StylePriority.UsePadding = false;
      this.lineTotal.StylePriority.UseTextAlignment = false;
      this.lineTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.lineTotal.TextFormatString = "{0:#,#0}";
      this.lineTotal.Weight = 0.36856596751285864D;
      // 
      // unitPrice
      // 
      this.unitPrice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.unitPrice.Dpi = 254F;
      this.unitPrice.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Price]")});
      this.unitPrice.Name = "unitPrice";
      this.unitPrice.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 13, 0, 254F);
      this.unitPrice.StylePriority.UseBorders = false;
      this.unitPrice.StylePriority.UsePadding = false;
      this.unitPrice.StylePriority.UseTextAlignment = false;
      this.unitPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.unitPrice.TextFormatString = "{0:#,#}";
      this.unitPrice.Weight = 0.28053347542437074D;
      // 
      // productName
      // 
      this.productName.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.productName.Dpi = 254F;
      this.productName.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SP_GET_TotalProductAmount].[InStockCapacity]")});
      this.productName.Name = "productName";
      this.productName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 13, 0, 254F);
      this.productName.StylePriority.UseBorders = false;
      this.productName.StylePriority.UsePadding = false;
      this.productName.StylePriority.UseTextAlignment = false;
      this.productName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.productName.Weight = 0.13494545526787261D;
      // 
      // quantity
      // 
      this.quantity.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.quantity.Dpi = 254F;
      this.quantity.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SP_GET_TotalProductAmount].[Code]")});
      this.quantity.Name = "quantity";
      this.quantity.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 13, 0, 254F);
      this.quantity.StylePriority.UseBorders = false;
      this.quantity.StylePriority.UsePadding = false;
      this.quantity.StylePriority.UseTextAlignment = false;
      this.quantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.quantity.Weight = 0.2236476068466603D;
      // 
      // xrTableCell3
      // 
      this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell3.Dpi = 254F;
      this.xrTableCell3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SP_GET_TotalProductAmount].[Name]")});
      this.xrTableCell3.Multiline = true;
      this.xrTableCell3.Name = "xrTableCell3";
      this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 13, 0, 254F);
      this.xrTableCell3.StylePriority.UseBorders = false;
      this.xrTableCell3.StylePriority.UsePadding = false;
      this.xrTableCell3.StylePriority.UseTextAlignment = false;
      this.xrTableCell3.Text = "xrTableCell3";
      this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.xrTableCell3.Weight = 1.0560967381402615D;
      // 
      // xrTableCell4
      // 
      this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
      this.xrTableCell4.Dpi = 254F;
      this.xrTableCell4.Multiline = true;
      this.xrTableCell4.Name = "xrTableCell4";
      this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 13, 0, 254F);
      this.xrTableCell4.StylePriority.UseBorders = false;
      this.xrTableCell4.StylePriority.UsePadding = false;
      this.xrTableCell4.StylePriority.UseTextAlignment = false;
      this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
      this.xrTableCell4.Weight = 0.14526106310451156D;
      // 
      // detailTableRow
      // 
      this.detailTableRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell3,
            this.quantity,
            this.productName,
            this.unitPrice,
            this.lineTotal});
      this.detailTableRow.Dpi = 254F;
      this.detailTableRow.Name = "detailTableRow";
      this.detailTableRow.Weight = 10.58D;
      // 
      // detailTable
      // 
      this.detailTable.Dpi = 254F;
      this.detailTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.detailTable.Name = "detailTable";
      this.detailTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.detailTableRow});
      this.detailTable.SizeF = new System.Drawing.SizeF(1648.46F, 58.42F);
      this.detailTable.StylePriority.UseFont = false;
      // 
      // ProductAmountReport
      // 
      this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.GroupHeader2,
            this.GroupFooter1,
            this.GroupHeader1,
            this.GroupFooter2});
      this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.efDataSourceProductAmount});
      this.DataMember = "SP_GET_TotalProductAmount";
      this.DataSource = this.efDataSourceProductAmount;
      this.Dpi = 254F;
      this.Margins = new System.Drawing.Printing.Margins(210, 239, 76, 201);
      this.PageHeight = 2970;
      this.PageWidth = 2100;
      this.PaperKind = System.Drawing.Printing.PaperKind.A4;
      this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
      this.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
      this.SnapGridSize = 25F;
      this.Version = "18.2";
      this.Watermark.Font = new System.Drawing.Font("Times New Roman", 36F);
      this.Watermark.ForeColor = System.Drawing.Color.Silver;
      this.Watermark.Text = "Son Ngoc Chau";
      ((System.ComponentModel.ISupportInitialize)(this.invoiceInfoTable)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.customerTable)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.totalTable)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.headerTable)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.efDataSourceProductAmount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.detailTable)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
    private DevExpress.XtraReports.UI.XRTable invoiceInfoTable;
    private DevExpress.XtraReports.UI.XRTableRow invoiceInfoTableRow1;
    private DevExpress.XtraReports.UI.XRTable customerTable;
    private DevExpress.XtraReports.UI.XRTableRow customerCountryRow;
    private DevExpress.XtraReports.UI.XRTableCell customerCountry;
    private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
    private DevExpress.XtraReports.UI.XRTable totalTable;
    private DevExpress.XtraReports.UI.XRTableRow totalRow;
    private DevExpress.XtraReports.UI.XRTableCell totalCaption;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
    private DevExpress.XtraReports.UI.XRTable headerTable;
    private DevExpress.XtraReports.UI.XRTableRow headerTableRow;
    private DevExpress.XtraReports.UI.XRTableCell quantityCaption;
    private DevExpress.XtraReports.UI.XRTableCell productNameCaption;
    private DevExpress.XtraReports.UI.XRTableCell unitPriceCaption;
    private DevExpress.XtraReports.UI.XRTableCell lineTotalCaption;
    private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
    private DevExpress.XtraReports.UI.XRLabel thankYouLabel;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
    private DevExpress.XtraReports.UI.XRTableCell total;
    private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    private DevExpress.XtraReports.UI.XRLabel xrLabel3;
    private DevExpress.XtraReports.UI.XRLabel xrLabel2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
    private DevExpress.XtraReports.UI.XRTableCell invoiceLabel;
        private DevExpress.DataAccess.EntityFramework.EFDataSource efDataSourceProductAmount;
    private DevExpress.XtraReports.UI.XRTable detailTable;
    private DevExpress.XtraReports.UI.XRTableRow detailTableRow;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
    private DevExpress.XtraReports.UI.XRTableCell quantity;
    private DevExpress.XtraReports.UI.XRTableCell productName;
    private DevExpress.XtraReports.UI.XRTableCell unitPrice;
    private DevExpress.XtraReports.UI.XRTableCell lineTotal;
  }
}
