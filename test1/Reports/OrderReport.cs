﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace test1.Reports
{
  public partial class OrderReport : DevExpress.XtraReports.UI.XtraReport
  {
    ReportParameter reportParameter;

    public OrderReport()
    {
      InitializeComponent();
    }
    public OrderReport(ReportParameter reportParameter)
    {
      this.reportParameter = reportParameter;
      InitializeComponent();
      Init(reportParameter.Id);
    }

    public void Init(int orderId)
    {

        this.efDataSource1.StoredProcedures.Clear();
        DevExpress.DataAccess.EntityFramework.EFConnectionParameters efConnectionParameters1 = new DevExpress.DataAccess.EntityFramework.EFConnectionParameters();
        DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo efStoredProcedureInfo1 = new DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo();
        DevExpress.DataAccess.EntityFramework.EFParameter efParameter1 = new DevExpress.DataAccess.EntityFramework.EFParameter();

        efConnectionParameters1.ConnectionStringName = "PaintManagementContext";
        efConnectionParameters1.Source = typeof(test1.PaintManagementContext);
        this.efDataSource1.ConnectionParameters = efConnectionParameters1;
        this.efDataSource1.Name = "efDataSource1";
        efStoredProcedureInfo1.Name = "SP_GET_ORDER_REPORT";
        efParameter1.Name = "OrderId";
        efParameter1.Type = typeof(int);
        efParameter1.ValueInfo = orderId.ToString();
        efStoredProcedureInfo1.Parameters.AddRange(new DevExpress.DataAccess.EntityFramework.EFParameter[] {
        efParameter1});
        this.efDataSource1.StoredProcedures.AddRange(new DevExpress.DataAccess.EntityFramework.EFStoredProcedureInfo[] {
        efStoredProcedureInfo1});


    }

  }
}
