﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AutoMapper;
using AutoMapper.Configuration;
using DevExpress.LookAndFeel;
using Newtonsoft.Json;
using SimpleInjector;
using test1.Services;
using test1.ViewModels;

namespace test1
{
  static class Program
  {

    private static Container container;

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Bootstrap();
      //AddMapper();
     
      Application.Run(container.GetInstance<MainForm>());
    }
    private static void Bootstrap()
    {
      // Create the container as usual.
      container = new Container();

      // Register your types, for instance:
      container.Register<CustomerService>(Lifestyle.Singleton);
      container.Register<ProductService>(Lifestyle.Singleton);
      container.Register<OrderService>(Lifestyle.Singleton);
      container.Register<StockService>(Lifestyle.Singleton);
      container.Register<ReportService>(Lifestyle.Singleton);
      var connectionString = GetConnetionString();
      container.Register<PaintManagementContext>(() => new PaintManagementContext(connectionString), Lifestyle.Singleton);

      // Automapper
      container.RegisterSingleton(() => GetMapper(container));
      container.Register<MainForm>(Lifestyle.Singleton);


      // Optionally verify the container.
      container.Verify();
    }

    private static AutoMapper.IMapper GetMapper(Container container)
    {
      var mp = container.GetInstance<Cores.Mapping.MapperProvider>();
      return mp.GetMapper();
    }
    private static void AddMapper()
    {
      AutoMapper.Mapper.Initialize(cfg => {
        cfg.AllowNullCollections = true;
        //cfg.CreateMap<Customer, CustomerViewModel>();
        //cfg.CreateMap<CustomerViewModel, Customer>();

        //cfg.CreateMap<Customer, CustomerGridView>();
        //cfg.CreateMap<List<Customer>, List<CustomerGridView>>();

      });
    }

    private static string GetConnetionString()
    {
      using (StreamReader file = File.OpenText(@"app.json"))
      {
        JsonSerializer serializer = new JsonSerializer();
        var config = JsonConvert.DeserializeObject<Config>(file.ReadToEnd());
        return $"data source={config.ConnectionString.ServerName};initial catalog={config.ConnectionString.DBName};integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";
      }

    }

    public class Config
    {
      public ConnectionString ConnectionString { get; set; }
    }
    public class ConnectionString
    {
      public string ServerName { get; set; }
      public string DBName { get; set; }
    }

  }
}
