﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.Services
{
  public class StockService
  {
    PaintManagementContext PaintManagementContext;

    public StockService(PaintManagementContext context)
    {
      PaintManagementContext = context;
    }

    public Stock AddOrUpdate(Stock stock)
    {
      return stock.Id == 0 ? Add(stock) : Update(stock);
    }

    public Stock Add(Stock stock)
    {
      var data = this.PaintManagementContext.Stocks.Add(stock);
      this.PaintManagementContext.SaveChanges();
      return data;
    }

    public Stock Update(Stock stock)
    {
      var entity = PaintManagementContext.Stocks.Find(stock.Id);
      PaintManagementContext.Entry(entity).CurrentValues.SetValues(stock);
      this.PaintManagementContext.SaveChanges();
      return stock;
    }

    public int Delete(Stock stock)
    {
      var entity = PaintManagementContext.Stocks.Find(stock.Id);
      if (entity != null)
      {
        this.PaintManagementContext.Stocks.Remove(entity);
        return this.PaintManagementContext.SaveChanges();
      }
      return 0;
    }

    public List<Stock> GetStocks()
    {
      return PaintManagementContext.Stocks.Include("Product").ToList();
    }

    public List<Product> GetAllProducts()
    {
      return PaintManagementContext.Products.ToList();
    }

    public Stock GetStockById(int id)
    {
      return PaintManagementContext.Stocks.FirstOrDefault(c => c.Id == id);
    }

    public List<Stock> SearchStock(string data)
    {
      if (!string.IsNullOrEmpty(data))
        data = data.ToLower();

      var customers = this.PaintManagementContext.Stocks.Where(c => data == "" || c.Product.Name.ToLower().Contains(data) || c.Product.Code.ToLower().Contains(data)).ToList();
      return customers;
    }

  }
}
