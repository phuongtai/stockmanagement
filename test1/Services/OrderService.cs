﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test1.ViewModels;

namespace test1.Services
{
  public class OrderService
  {

    PaintManagementContext PaintManagementContext;
    private IMapper mapper;

    public OrderService(PaintManagementContext context, IMapper mapper)
    {
      this.PaintManagementContext = context;
      this.mapper = mapper;
    }

    public OrderViewModel Save(OrderViewModel order, List<ProductOrderViewModel> products)
    {
      var data = mapper.Map<Order>(order);

      if (order.ID > 0)
      {
        var productOrders = this.PaintManagementContext.ProductOrders.Where(p => p.OrderId == order.ID).ToList();
        this.PaintManagementContext.ProductOrders.RemoveRange(productOrders);

        var entity = PaintManagementContext.Orders.Find(order.ID);
        PaintManagementContext.Entry(entity).CurrentValues.SetValues(data);
      }
      else this.PaintManagementContext.Orders.Add(data);

      foreach (var product in products)
      {
        var productOrder = mapper.Map<ProductOrder>(product);
        productOrder.OrderId = data.Id;
        this.PaintManagementContext.ProductOrders.Add(productOrder);
      }
      this.PaintManagementContext.SaveChanges();
      order.ID = data.Id;
      return order;
    }

    public List<OrderHistoryViewModel> GetOderHistory()
    {
      var data = PaintManagementContext.Database.SqlQuery<SP_GET_ORDER_HISTORY_Result>("SP_GET_ORDER_HISTORY").ToList();
      return mapper.Map<List<OrderHistoryViewModel>>(data);
    }

    public OrderViewModel GetOderById(int orderId)
    {
      var data = PaintManagementContext.Orders.FirstOrDefault(x => x.Id == orderId && !x.IsDeleted);
      var results = mapper.Map<OrderViewModel>(data);
      return results;
    }

  }
}
