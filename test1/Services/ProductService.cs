﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.Services
{
  public class ProductService
  {
    PaintManagementContext PaintManagementContext;

    public List<Unit> GetUnits()
    {
      return PaintManagementContext.Units.ToList();
    }

    public ProductService(PaintManagementContext context)
    {
      this.PaintManagementContext = context;
    }

    public Product AddOrUpdate(Product product)
    {
      return product.Id == 0 ? Add(product) : Update(product);
    }

    public Product Add(Product product)
    {
      var data = this.PaintManagementContext.Products.Add(product);
      this.PaintManagementContext.SaveChanges();
      return data;
    }

    public Product Update(Product product)
    {
      var entity = PaintManagementContext.Products.Find(product.Id);
      PaintManagementContext.Entry(entity).CurrentValues.SetValues(product);
      this.PaintManagementContext.SaveChanges();
      return product;
    }

    public int Delete(Product product)
    {
      var entity = PaintManagementContext.Products.Find(product.Id);
      if (entity != null)
      {
        this.PaintManagementContext.Products.Remove(entity);
        return this.PaintManagementContext.SaveChanges();
      }
      return 0;
    }

    public bool CanDelete(int id)
    {
      var data = PaintManagementContext.Products.Count(cus => cus.ProductOrders.Any(sale => sale.ProductId == id));
      return data == 0;
    }

    public List<Product> GetProducts()
    {
      return PaintManagementContext.Products.Include("Unit").ToList();
    }

    public List<Product> SearchProduct(string data)
    {
      if (!string.IsNullOrEmpty(data))
        data = data.ToLower();

      var products = this.PaintManagementContext.Products.Where(p => data == "" || p.Name.ToLower().Contains(data) || p.Code.ToLower().Contains(data)).ToList();
      return products;
    }

  }
}
