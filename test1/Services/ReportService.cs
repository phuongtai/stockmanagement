﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.Services
{
  public class ReportService
  {
    PaintManagementContext PaintManagementContext;

    public ReportService(PaintManagementContext context)
    {
      PaintManagementContext = context;
    }
     
    public List<SP_GET_ProductInStock_Result> GetDataReport()
    {
      return PaintManagementContext.Database.SqlQuery<SP_GET_ProductInStock_Result>("SP_GET_ProductInStock").ToList();
    }

    public List<Order> GetAllOrders()
    {
      return PaintManagementContext.Orders.Include("Customer").ToList();
    }

    public List<ProductOrder> GetAllSaleProducts()
    {
      return PaintManagementContext.ProductOrders.ToList();
    }

    public List<SP_GET_ProductInStock_Result> SearchProduct(string value)
    {
      var items = PaintManagementContext.Database.SqlQuery<SP_GET_ProductInStock_Result>("SP_GET_ProductInStock").ToList();
      if (!string.IsNullOrEmpty(value))
      {
        var lowerValue = value.ToLower();
        var datas = items.Where(x => x.Name.ToLower().Contains(lowerValue) || x.Code.ToLower().Contains(lowerValue)).ToList();
        return datas;
      }
      return items;
    }

  }
}
