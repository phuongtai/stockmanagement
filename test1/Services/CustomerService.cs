﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.Services
{
  public class CustomerService
  {
    PaintManagementContext PaintManagementContext;

    public CustomerService(PaintManagementContext context)
    {
      PaintManagementContext = context;
    }

    public Customer AddOrUpdate(Customer customer)
    {
      return customer.Id == 0 ? Add(customer) : Update(customer);
    }

    public Customer Add(Customer customer)
    {
      var data = this.PaintManagementContext.Customers.Add(customer);
      this.PaintManagementContext.SaveChanges();
      return data;
    }

    public Customer Update(Customer customer)
    {
      var entity = PaintManagementContext.Customers.Find(customer.Id);
      PaintManagementContext.Entry(entity).CurrentValues.SetValues(customer);
      this.PaintManagementContext.SaveChanges();
      return customer;
    }

    public int Delete(Customer customer)
    {
      var entity = PaintManagementContext.Customers.Find(customer.Id);
      if (entity != null)
      {
        this.PaintManagementContext.Customers.Remove(entity);
        return this.PaintManagementContext.SaveChanges();
      }
      return 0;
    }

    public List<Customer> GetCustomers()
    {
      return PaintManagementContext.Customers.ToList();
    }

    public Customer GetCustomerById(int id)
    {
      return PaintManagementContext.Customers.FirstOrDefault(c => c.Id == id);
    }

    public bool CanDelete(int id)
    {
      var data = PaintManagementContext.Customers.Count(cus => cus.Orders.Any(sale => sale.CustomerId == id));
      return data == 0;
    }

    public List<Customer> SearchCustomer(string data)
    {
      if(!string.IsNullOrEmpty(data))
        data = data.ToLower();

      var customers = this.PaintManagementContext.Customers.Where(c => data == "" || c.Name.ToLower().Contains(data) || c.Phone.ToLower().Contains(data)).ToList();
      return customers;
    }

  }
}
