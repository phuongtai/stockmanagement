﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using test1.Reports;
using DevExpress.XtraReports.UI;
using DevExpress.DataAccess.Sql;

namespace test1
{
  public partial class ReportViewer : DevExpress.XtraEditors.XtraForm
  {
    ReportParameter reportParameter;

    public ReportViewer(ReportParameter reportParameter)
    {
      this.reportParameter = reportParameter;
      InitializeComponent();
    }

    public void ShowProductOrderReport()
    {
      OrderReport report = new OrderReport(reportParameter);
      this.documentViewer1.DocumentSource = report;

      this.Show();
    }

    public void ShowProductAmounReport()
    {
      var report = new ProductAmountReport(reportParameter);
      this.documentViewer1.DocumentSource = report;
      this.Show();
    }

  }
}