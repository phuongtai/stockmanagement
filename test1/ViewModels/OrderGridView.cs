﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Office;
using test1.Extendtion;

namespace test1.ViewModels
{
  public class OrderGridView  
  {
    [ReadOnly(true)]
    [Display(Order = -1)]
    public int ID { get; set; }

    [Display(ShortName = "Tên Khách Hàng", Name = "Tên Khách Hàng")]
    public string CustomerName { get; set; }

    [Display(ShortName = "Ngày Bán Hàng", Name = "Ngày Bán Hàng")]
    public System.DateTime CreatedDate { get; set; }

    [Display(ShortName = "Ghi Chú", Name = "Ghi Chú")]
    public string Note { get; set; }
     
   
  }
}
