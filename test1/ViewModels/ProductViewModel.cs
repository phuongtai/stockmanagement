﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Office;
using test1.Extendtion;

namespace test1.ViewModels
{
  public class ProductViewModel : ICloneable, IViewModel
  {

    public int ID { get; set; }

    [Required(ErrorMessage = "Nhập vào tên sản phẩm")]
    [StringLength(500)]
    public string Name { get; set; }

    [StringLength(20)]
    public string Code { get; set; }

    [Required(ErrorMessage = "Nhập vào giá bán")]
    public decimal Price { get; set; }

    [Required(ErrorMessage = "Hãy chọn đơn vị tính")]
    public short UnitId { get; set; }

    public DateTime LastChanged { get; set; }

    public object Clone()
    {
      return this.CloneObject();
    }

   
  }
}
