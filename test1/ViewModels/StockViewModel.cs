﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Office;
using test1.Extendtion;

namespace test1.ViewModels
{
  public class StockViewModel : ICloneable, IViewModel
  {

    public int ID { get; set; }

    [Range(1, int.MaxValue,
        ErrorMessage = "Hãy chọn Product")]
    public int ProductId { get; set; }

    public string ProductName { get; set; }

    public string Code { get; set; }

    [Required(ErrorMessage = "Nhập vào số lượng nhập")]
    [Range(1, int.MaxValue, ErrorMessage = "Nhập vào số lượng nhập")]
    public int Quantity { get; set; }

    [Required(ErrorMessage = "Nhập vào giá bán")]
    [Range(0, int.MaxValue, ErrorMessage = "Nhập vào giá bán")]
    public decimal PriceImport { get; set; }

    [Required(ErrorMessage = "Nhập vào ngày nhập hàng")]
    public DateTime CreatedDate { get; set; }

    public string Note { get; set; }

    public object Clone()
    {
      return this.CloneObject();
    }

   
  }
}
