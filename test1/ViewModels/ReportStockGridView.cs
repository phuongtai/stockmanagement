﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.ViewModels
{
  public class ReportStockGridView
  {

    [Display(ShortName = "Tên Sản Phẩm", Name = "Sản Phẩm")]
    public string Name { get; set; }

    [Display(ShortName = "Mã Sản Phẩm", Name = "Code")]
    public string Code { get; set; }

    [Display(ShortName = "Số Lượng Bán", Name = "Số Lượng Bán")]
    public int SaleCapacity  { get; set; }

    [Display(ShortName = "Số Lượng Nhập", Name = "Số Lượng Nhập")]
    public int ImportCapacity { get; set; }

    [Display(ShortName = "Số Lượng Tồn", Name = "Số Lượng Tồn")]
    public int InStockCapacity { get; set; }

  }
}
