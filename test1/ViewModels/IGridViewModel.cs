﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.ViewModels
{
  public interface IGridViewModel
  {
    int ID { get; set; }
    string Name { get; set; }
    string Code { get; set; }
  }
}
