﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.ViewModels
{
  public class CustomerGridView 
  {
    [ReadOnly(true)]
    [Display(Order = -1)]
    public int ID { get; set; }

    [Display(ShortName = "Tên Khách hàng", Name = "Khách hàng")]
    public string Name { get; set; }

    [Display(ShortName = "Điện thoại")]
    public string Phone { get; set; }

    [Display(ShortName = "Địa chỉ", Name = "Địa chỉ")]
    public string Address { get; set; }

    [Display(ShortName = "Email")]
    public string Email { get; set; }

    [DataType(DataType.Text), Display(Order = -1)]
    public DateTime CreatedDate { get; set; }
  }
}
