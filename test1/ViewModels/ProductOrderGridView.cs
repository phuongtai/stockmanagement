﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.ViewModels
{
  public class ProductOrderGridView
  {
    [ReadOnly(true)]
    [Display(Order = -1)]
    public int OrderId { get; set; }

    [Display(ShortName = "Tên Sản Phẩm", Name = "Sản Phẩm")]
    public string Name { get; set; }

    [DataType(DataType.Currency), Display(ShortName = "Đơn Giá", Name = "Đơn Giá")]
    public decimal Price { get; set; }

    [DataType(DataType.Text), Display(ShortName = "Số Lượng")]
    public int? Quantity { get; set; }

    [DataType(DataType.Currency), Display(ShortName = "Thành Tiền")]
    public decimal Total { get; set; }
  }
}
