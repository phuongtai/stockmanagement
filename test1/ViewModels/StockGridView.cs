﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.ViewModels
{
  public class StockGridView  
  {
    [ReadOnly(true)]
    [Display(Order = -1)]
    public int ID { get; set; }

    [Display(ShortName = "Tên Sản Phẩm", Name = "Sản Phẩm")]
    public string ProductName { get; set; }

    [Display(ShortName = "Mã Sản Phẩm")]
    public string Code { get; set; }

    [Display(ShortName = "Số Lượng Nhập", Name = "Số Lượng Nhập")]
    public int Quantity { get; set; }

    [DataType(DataType.Currency), Display(ShortName = "Giá Nhập", Name = "Giá Nhập")]
    public decimal Price { get; set; }

    [Display(ShortName = "Ghi Chú", Name = "Ghi Chú", Order = -1)]
    public string Note { get; set; }

    [Display(ShortName = "Ngày Nhập", Name = "Ngày Nhập")]
    public DateTime CreatedDate { get; set; }

    [ReadOnly(true)]
    [Display(Order = -1)]
    public int ProductId { get; set; }
  }
}
