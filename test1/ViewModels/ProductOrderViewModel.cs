﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.ViewModels
{
  public class ProductOrderViewModel
  {
    [ReadOnly(true)]
    [Display(Order = -1)]
    public int OrderId { get; set; }

    [ReadOnly(true)]
    [Display(Order = -1)]
    public int ProductId { get; set; }

    [ReadOnly(true)]
    [Display(Order = -1)]
    public decimal OriginalPrice { get; set; }

    [ReadOnly(true)]
    [Display(Order = -1)]
    public int? PricePercent { get; set; }

    [Display(ShortName = "Tên Sản Phẩm", Name = "Sản Phẩm")]
    public string Name { get; set; }

    [Display(ShortName = "Mã Sản Phẩm", Name = "Mã Sản Phẩm")]
    public string Code { get; set; }

    [DataType(DataType.Currency), Display(ShortName = "Đơn Giá", Name = "Đơn Giá")]
    public decimal Price { get; set; }

    [DataType(DataType.Text), Display(ShortName = "Số Lượng")]
    public int? Quantity { get; set; }

    [DataType(DataType.Currency), Display(ShortName = "Thành Tiền")]
    public decimal Total { get; set; }

  }
}
