﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Office;
using test1.Extendtion;

namespace test1.ViewModels
{
  public class OrderHistoryViewModel
  {
    //////[ReadOnly(true)]
    //////[Display(Order = -1)]
    [Display(ShortName = "Click", Name = "Click")]
    public int OrderId { get; set; }

    [Display(ShortName = "STT", Name = "STT")]
    public int STT { get; set; }

    [Display(ShortName = "Ngày Lập", Name = "Ngày Lập")]
    public DateTime CreatedDate { get; set; }

    [Display(ShortName = "Tên Khách Hàng", Name = "Tên Khách Hàng")]
    public string CustomerName { get; set; }

    [Display(ShortName = "Tổng Tiền", Name = "Tổng Tiền")]
    public Decimal Total { get; set; }

    [Display(ShortName = "Ghi Chú", Name = "Ghi Chú")]
    public string Note { get; set; }
  }
}
