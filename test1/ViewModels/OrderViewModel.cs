﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Office;
using test1.Extendtion;

namespace test1.ViewModels
{
  public class OrderViewModel : ICloneable, IViewModel
  {

    public int ID { get; set; }

    public int CustomerId { get; set; }
    public System.DateTime CreatedDate { get; set; }
    public string Note { get; set; }
    public bool IsDeleted { get; set; }

    public List<ProductOrderViewModel> Products { get; set; }

    public object Clone()
    {
      return this.CloneObject();
    }

   
  }
}
