﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Office;
using test1.Extendtion;

namespace test1.ViewModels
{
  public class CustomerViewModel : ICloneable, IViewModel
  {

    public int ID { get; set; }

    [Required(ErrorMessage = "Nhập vào tên khách hàng")]
    [StringLength(500)]
    public string Name { get; set; }

    [StringLength(20)]
    public string Phone { get; set; }

    [StringLength(200)]
    public string Address { get; set; }

    [StringLength(50)]
    public string Email { get; set; }

    public DateTime CreatedDate { get; set; }

    public object Clone()
    {
      return this.CloneObject();
    }

   
  }
}
