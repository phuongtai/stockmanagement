﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.ViewModels
{
  public class ProductGridView: IGridViewModel
  {
    [ReadOnly(true)]
    [Display(Order = -1)]
    public int ID { get; set; }

    [Display(ShortName = "Tên Sản Phẩm", Name = "Sản Phẩm")]
    public string Name { get; set; }

    [Display(ShortName = "Mã Sản Phẩm")]
    public string Code { get; set; }

    [DataType(DataType.Currency), Display(ShortName = "Đơn Giá", Name = "Đơn Giá")]
    public decimal Price { get; set; }

    [Display(ShortName = "Đơn Vị Tính")]
    public string Unit { get; set; }

    [DataType(DataType.Text), Display(Order = -1)]
    public DateTime LastChanged { get; set; }

    [Display(Order = -1)]
    public short UnitId { get; set; }
  }
}
