﻿using AutoMapper;
using AutoMapper.Configuration;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using test1.Profiles;

namespace test1.Cores.Mapping
{
  public class MapperProvider
  {
    private readonly Container _container;

    public MapperProvider(Container container)
    {
      _container = container;
    }

    public IMapper GetMapper()
    {
      var mce = new MapperConfigurationExpression();
      mce.ConstructServicesUsing(_container.GetInstance);

      mce.AddProfiles(typeof(CustomerGridViewProfile).Assembly);
      var mc = new MapperConfiguration(mce);
      mc.AssertConfigurationIsValid();

      IMapper m = new Mapper(mc, t => _container.GetInstance(t));

      return m;
    }
  }
}
