﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test1.ViewModels;

namespace test1.Cores.Mapping.Profiles
{
  public class MyProfile: Profile
  {
    public MyProfile()
    {
      CreateMap<Customer, CustomerViewModel>();
      CreateMap<CustomerViewModel, Customer>().ForMember(x => x.Orders, opt => opt.Ignore());

      // product
      CreateMap<Product, ProductViewModel>();
      CreateMap<ProductViewModel, Product>()
        .ForMember(x => x.ProductOrders, opt => opt.Ignore())
        .ForMember(x => x.Unit, opt => opt.Ignore())
        .ForMember(x => x.Stocks, opt => opt.Ignore());

      CreateMap<Product, ProductGridView>()
        .ForMember(x => x.Unit, opt => opt.MapFrom(x => x.Unit.Label))
        .ForMember(x => x.UnitId, opt => opt.MapFrom(x => x.Unit.Id));

      CreateMap<ProductGridView, ProductViewModel>();

      // Order
      CreateMap<ProductGridView, ProductOrderViewModel>()
        .ForMember(x => x.PricePercent, opt => opt.Ignore())
        .ForMember(x => x.ProductId, opt => opt.MapFrom(d => d.ID))
        .ForMember(x => x.OriginalPrice, opt => opt.MapFrom(d => d.Price))
        .ForMember(x => x.OrderId, opt => opt.Ignore())
        .ForMember(x => x.Quantity, opt => opt.Ignore())
        .ForMember(x => x.Total, opt => opt.Ignore());

      CreateMap<OrderViewModel, Order>()
        .ForMember(x => x.ProductOrders, opt => opt.Ignore())
        .ForMember(x => x.Customer, opt => opt.Ignore());

      CreateMap<Order, OrderViewModel>()
        .ForMember(x => x.Products, opt => opt.MapFrom(x => x.ProductOrders));

      CreateMap<ProductOrderViewModel, ProductOrder>()
        .ForMember(x => x.OriginalPrice, opt => opt.MapFrom(x => x.OriginalPrice))
        .ForMember(x => x.Order, opt => opt.Ignore())
        .ForMember(x => x.Product, opt => opt.Ignore());

      CreateMap<ProductOrder, ProductOrderViewModel>()
       .ForMember(x => x.PricePercent, opt => opt.MapFrom(x => x.PricePercent))
       .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Product.Name))
       .ForMember(x => x.Code, opt => opt.MapFrom(x => x.Product.Code));

      // Stock
      CreateMap<Stock, StockViewModel>()
        .ForMember(x => x.ProductName, opt => opt.MapFrom(p => p.Product.Name))
        .ForMember(x => x.Code, opt => opt.MapFrom(p => p.Product.Code))
        .ForMember(x => x.PriceImport, opt => opt.MapFrom(p => p.Price));
      CreateMap<StockViewModel, Stock>()
       .ForMember(x => x.Price, opt => opt.MapFrom(p => p.PriceImport))
       .ForMember(x => x.Year, opt => opt.Ignore())
       .ForMember(x => x.Month, opt => opt.Ignore())
       .ForMember(x => x.Product, opt => opt.Ignore());

      CreateMap<Stock, StockGridView>()
        .ForMember(x => x.ProductName, opt => opt.MapFrom(p => p.Product.Name))
        .ForMember(x => x.Code, opt => opt.MapFrom(p => p.Product.Code));

    CreateMap<StockGridView, StockViewModel>()
        .ForMember(x => x.PriceImport, opt => opt.MapFrom(p => p.Price));

      // Report
      CreateMap<SP_GET_ProductInStock_Result, ReportStockGridView>();
      CreateMap<ProductOrder, ProductOrderGridView>()
        .ForMember(x => x.Name, opt => opt.MapFrom(p => p.Product.Name));
      CreateMap<Order, OrderGridView>()
        .ForMember(x => x.CustomerName, opt => opt.MapFrom(p => p.Customer.Name));

      CreateMap<SP_GET_ORDER_HISTORY_Result, OrderHistoryViewModel>();
    }
  }
}
