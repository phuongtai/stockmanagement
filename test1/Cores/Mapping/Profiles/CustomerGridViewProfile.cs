﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test1.ViewModels;

namespace test1.Profiles
{
  public class CustomerGridViewProfile: Profile
  {
    public CustomerGridViewProfile()
    {
      CreateMap<Customer, CustomerGridView>();
      CreateMap<CustomerGridView, Customer>().ForMember(x => x.Orders, opt => opt.Ignore());

    }
  }
}
