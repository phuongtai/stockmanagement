﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.Extendtion
{
  public static class Extentions
  {
    public static T CloneObject<T>(this T source)
    {
      var serialized = JsonConvert.SerializeObject(source);
      return JsonConvert.DeserializeObject<T>(serialized);
    }
  }
}
