﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using test1.ViewModels;

namespace test1.Cores
{
  public class BaseForm : DevExpress.XtraEditors.XtraForm
  {
    public virtual bool Validation(IViewModel data)
    {
      var message = "";
      ValidationContext context = new ValidationContext(data, null, null);
      IList<ValidationResult> errors = new List<ValidationResult>();
      if (!Validator.TryValidateObject(data, context, errors, true))
      {
        foreach (ValidationResult result in errors)
        {
          message += result.ErrorMessage + "\n";
        }
      }

      if (!string.IsNullOrEmpty(message))
      {
        MessageBox.Show(message, "Warning", MessageBoxButtons.OK, icon: MessageBoxIcon.Error);
        return false;
      }

      return true;
    }
  }
}
