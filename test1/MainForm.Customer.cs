﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using test1.Services;
using test1.ViewModels;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using test1.Cores;
using AutoMapper;
using DevExpress.XtraGrid.Views.Grid;

namespace test1
{
  public partial class MainForm
  {

   
    private void LoadCustomerGridContrl()
    {
      this.gridView1.Columns.Clear();
      var customers = CustomerService.GetCustomers();

      var dataGrids = mapper.Map<List<CustomerGridView>>(customers);
      mainBindingSource.DataSource = dataGrids;
    }

    private void AddOrUpdateCustomer(CustomerViewModel customerViewModel)
    {
      if (!Validation(customerViewModel)) return;

      var data = mapper.Map<Customer>(customerViewModel);
      data.CreatedDate = DateTime.Now;
      var customer = this.CustomerService.AddOrUpdate(data);
      customerViewModel.ID = customer.Id;
      LoadCustomerGridContrl();
      this.alertControl1.Show(this, "Save data Success !", "");
    }

    private void DeleteCustomer(CustomerViewModel customerViewModel)
    {
      if (CustomerService.CanDelete(customerViewModel.ID))
      {
        var data = mapper.Map<Customer>(customerViewModel);
        this.CustomerService.Delete(data);
        this.alertControl1.Show(this, "Data has removed !", "");
      }
      else
      {
        MessageBox.Show("- Khách hàng này ko thể xóa. Vì khách hàng có tạo đơn hàng. !", "Cảnh Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

    }

    private void BindingCustomerControl(CustomerViewModel viewModel)
    {
      this.txtCustomer.DataBindings.Clear();
      this.txtCustomer.DataBindings.Add("Text", viewModel, "Name", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtPhone.DataBindings.Clear();
      this.txtPhone.DataBindings.Add("Text", viewModel, "Phone", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtAddress.DataBindings.Clear();
      this.txtAddress.DataBindings.Add("Text", viewModel, "Address", false, DataSourceUpdateMode.OnPropertyChanged);

      this.txtEmail.DataBindings.Clear();
      this.txtEmail.DataBindings.Add("Text", viewModel, "Email", false, DataSourceUpdateMode.OnPropertyChanged);
    }

    private void LoadCustomer(CustomerGridView customerGridView)
    {
      this.ViewModel = mapper.Map<CustomerViewModel>(customerGridView);

    }

    private void AddCustomerAction()
    {
      this.ViewModel = new CustomerViewModel();
      this.txtCustomer.Focus();
    }

  }
}